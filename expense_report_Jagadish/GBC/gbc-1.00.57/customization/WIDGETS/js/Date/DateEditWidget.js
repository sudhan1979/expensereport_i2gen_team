"use strict";

modulum('MyEditWidgetLogin', ['EditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyEditWidgetLogin = context.oo.Class(cls.EditWidget, function($super) {
      return {
        __name: "MyEditWidgetLogin",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Edit', 'date',cls.MyEditWidgetLogin);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
