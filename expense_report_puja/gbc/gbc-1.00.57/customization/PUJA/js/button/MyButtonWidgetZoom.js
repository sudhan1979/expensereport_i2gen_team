"use strict";

modulum('MyButtonWidgetZoom', ['ButtonWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyButtonWidgetZoom = context.oo.Class(cls.ButtonWidget, function($super) {
      return {
        __name: "MyButtonWidgetZoom",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Button', 'zoom-button',cls.MyButtonWidgetZoom);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
