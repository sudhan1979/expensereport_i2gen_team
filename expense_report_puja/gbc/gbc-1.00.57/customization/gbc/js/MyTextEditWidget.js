"use strict";

modulum('MyTextEditWidget', ['TextEditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyTextEditWidget = context.oo.Class(cls.MyTextEditWidget, function($super) {
      return {
        __name: "MyTextEditWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('htmlnobox',cls.MyTextEditWidget);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
