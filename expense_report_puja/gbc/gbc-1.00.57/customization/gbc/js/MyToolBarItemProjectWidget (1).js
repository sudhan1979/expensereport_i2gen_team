"use strict";

modulum('MyToolBarItemProjectWidget', ['ToolBarItemWidget', 'WidgetFactory'],
function(context, cls) {

cls.MyToolBarItemProjectWidget = context.oo.Class(cls.ToolBarItemWidget, function($super) {
return {
__name: "MyToolBarItemProjectWidget",

/* your custom code */
};
});
cls.WidgetFactory.register('ToolBarItemWidget', 'proj_toolbar',cls.MyToolBarItemProjectWidget);
});