"use strict";

modulum('MyGroupTitleWidget', ['GroupTitleWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyGroupTitleWidget = context.oo.Class(cls.GroupTitleWidget, function($super) {
      return {
        __name: "MyGroupTitleWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('GroupTitle', 'addbrand',cls.MyGroupTitleWidget);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
