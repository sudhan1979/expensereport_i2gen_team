"use strict";

modulum('MyTextEditWidget', ['TextEditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyTextEditWidget = context.oo.Class(cls.TextEditWidget, function($super) {
      return {
        __name: "MyTextEditWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('TextEdit', 'htmlnobox',cls.MyTextEditWidget);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
