IMPORT util
IMPORT FGL expe
DATABASE expense

DEFINE
    exphead_rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department
    END RECORD,
    arr_expense DYNAMIC ARRAY OF RECORD
        serial_no LIKE exp_details.serial_no,
        exp_id LIKE exp_details.exp_id,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        total_paid_by_employee DECIMAL,
        total_charge_to_four_js DECIMAL
    END RECORD
TYPE bussi RECORD
    serial_no LIKE business_details.serial_no,
    exp_id LIKE business_details.exp_id,
    date LIKE business_details.date,
    amount LIKE business_details.amount,
    company_name LIKE business_details.company_name,
    business_trans LIKE business_details.business_trans
END RECORD
DEFINE busn DYNAMIC ARRAY OF bussi
TYPE buss RECORD
    serial_no LIKE business_details.serial_no,
    exp_id LIKE business_details.exp_id,
    date LIKE business_details.date,
    amount LIKE business_details.amount,
    company_name LIKE business_details.company_name,
    business_trans LIKE business_details.business_trans
END RECORD
DEFINE totalcharge DECIMAL
DEFINE bush DYNAMIC ARRAY OF buss
DEFINE today_date DATE
DEFINE rpt VARCHAR(255)
DEFINE handl om.SaxDocumentHandler

DEFINE total DECIMAL(9, 2)
DEFINE totalc DECIMAL(9, 2)

CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "business Updated"
CONSTANT C_TAX = 0.575

MAIN
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
    --  CALL ui.Interface.loadStyles("styles")
    -- CALL ui.Interface.loadActionDefaults("actions")
    CALL ui.Form.setDefaultInitializer("initializeForm")
    CLOSE WINDOW SCREEN
    OPEN WINDOW welcome WITH FORM "main"
    MENU
        ON ACTION add
            -- CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = add_fun()
            IF ha_employee THEN
                CALL arr_expense.clear()
                CALL expense_inpupd()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
            -- ON ACTION AddExp
            -- CALL expense_inpupd()
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
        ON ACTION PRINT
            CALL print_fun()
        ON ACTION AddbusinessData
            CALL input_buss()
            -- CALL insert_buss()

            --CLEAR FORM
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW welcome
END MAIN

PUBLIC FUNCTION initializeForm(frm ui.Form) RETURNS()

    --CALL frm.loadToolBar("tableEdit")

END FUNCTION

FUNCTION add_fun()
    DEFINE
        id INTEGER,
        name STRING,
        --notes STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE exphead_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO exphead_rec.exp_id FROM exp_header
    IF exphead_rec.exp_id IS NULL OR exphead_rec.exp_id == 0 THEN
        LET exphead_rec.exp_id = 1
    END IF

    LET int_flag = FALSE
    INPUT BY NAME exphead_rec.exp_id,
        exphead_rec.emp_no,
        exphead_rec.emp_name,
        exphead_rec.position,
        exphead_rec.manager,
        exphead_rec.department,
        exphead_rec.notes,
        exphead_rec.charge_type
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        ON CHANGE emp_no
            SELECT *
                INTO exphead_rec.*
                FROM exp_header
                WHERE exp_id = exphead_rec.exp_id

            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        ON ACTION zoom
            CALL display_emplist()
                RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET exphead_rec.emp_no = id
                LET exphead_rec.emp_name = NAME
                LET exphead_rec.position = position
                LET exphead_rec.manager = manager
                LET exphead_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME emp_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id, emp_no, notes, charge_type)
        VALUES(exphead_rec.exp_id,
            exphead_rec.emp_no,
            exphead_rec.notes,
            exphead_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added-----Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause ON exp_header.exp_id

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
            || "exp_header.exp_id,"
            || "exp_header.emp_no, "
            || "exp_header.notes, "
            || "emp_mast.position, "
            || "emp_mast.manager, "
            || "emp_mast.department, "
            || "emp_mast.emp_name, "
            || "exp_header.charge_type "
            || "FROM exp_header, emp_mast, exp_details "
            || "WHERE exp_header.exp_id = exp_details.exp_id "
            || "AND "
            || where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs
            INTO exphead_rec.exp_id,
                exphead_rec.emp_no,
                exphead_rec.notes,
                exphead_rec.position,
                exphead_rec.manager,
                exphead_rec.department,
                exphead_rec.emp_name,
                exphead_rec.charge_type
    ELSE
        FETCH PREVIOUS emp_curs INTO exphead_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME exphead_rec.*
    CALL expense_fetch()
    CALL business_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            exp_id LIKE exp_details.exp_id,
            date LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            total_paid_by_employee DECIMAL,
            total_charge_to_four_js DECIMAL
        END RECORD

    IF exphead_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.exp_id,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs
            FROM exp_details
            WHERE exp_details.exp_id = exphead_rec.exp_id

    LET expense_cnt = 0
    CALL arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION business_fetch()
    DEFINE
        business_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            DATE LIKE business_details.date,
            amount LIKE business_details.amount,
            company_name LIKE business_details.company_name,
            business_trans LIKE business_details.business_trans
        --expense_id LIKE business_details.expense_id
        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM business_details
            WHERE business_details.exp_id = exphead_rec.exp_id

    LET business_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET business_cnt = business_cnt + 1
        LET busn[business_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sb_rec.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY arr_expense TO sa_rec.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_inpupd()
--    DEFINE
--        opflag CHAR(1),
--        expense_cnt, curr_pa SMALLINT
--
--    LET opflag = "U"
--
--    LET expense_cnt = arr_expense.getLength()
--
--    INPUT ARRAY arr_expense
--        WITHOUT DEFAULTS
--        FROM sa_rec.*
--        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
--        BEFORE INPUT
--
--        BEFORE ROW
--            LET curr_pa = ARR_CURR()
--            LET arr_expense[curr_pa].mileage = 0.575
--            LET opflag = "U"
--
--        BEFORE INSERT
--            LET opflag = "I"
--            SELECT MAX(serial_no) + 1
--                INTO arr_expense[curr_pa].serial_no
--                FROM exp_details
--            IF arr_expense[curr_pa].serial_no IS NULL
--                OR arr_expense[curr_pa].serial_no == 0 THEN
--                LET arr_expense[curr_pa].serial_no = 1
--            END IF
--            LET arr_expense[curr_pa].exp_id = exphead_rec.exp_id
--            LET arr_expense[curr_pa].amount = 0
--            LET arr_expense[curr_pa].date = TODAY
--
--        AFTER INSERT
--            CALL expense_insert(curr_pa)
--
--        BEFORE DELETE
--            CALL expense_delete(curr_pa)
--
--        ON ROW CHANGE
--            CALL expense_update(curr_pa)
--
--        BEFORE FIELD exp_id
--            IF opflag = "U" THEN
--                NEXT FIELD DATE
--            END IF
--        AFTER FIELD expense_type
--            IF (arr_expense[arr_curr()].expense_type MATCHES "miles") THEN
--                MESSAGE "Choose one expense_type"
--                CALL DIALOG.setFieldActive("amount", 0)
--                NEXT FIELD mileage
--            ELSE
--                CALL DIALOG.setFieldActive("amount", 1)
--                NEXT FIELD amount
--
--            END IF
--
--
--        ON CHANGE mileage
--            WHENEVER ERROR CONTINUE
--            LET arr_expense[arr_curr()].amount =
--                C_TAX * arr_expense[arr_curr()].mileage
--
--            WHENEVER ERROR STOP
--            IF (SQLCA.SQLCODE = 0) THEN
--                DISPLAY BY NAME arr_expense[arr_curr()].*
--                --CALL DIALOG.setFieldActive("amount",FALSE)
--            END IF
--
--        AFTER FIELD charge_to_fourjs
--            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
--                MESSAGE "You must enter a value"
--                NEXT FIELD charge_to_fourjs
--            END IF
--
--            -- WHENEVER ERROR CONTINUE
--            LET arr_expense[arr_curr()].total_charge_to_four_js =
--                arr_expense[arr_curr()].amount
--                    + arr_expense[arr_curr()].charge_to_fourjs
--            DISPLAY "Total Charged ",
--                arr_expense[arr_curr()].total_charge_to_four_js
--
--            -- AFTER ROW
--            LET arr_expense[arr_curr()].total_paid_by_employee =
--                arr_expense[arr_curr()].total_charge_to_four_js
--            ---- AFTER INPUT --  DISPLAY "Total Charged ",recod[arr_curr()].total_charged_to_fourjs
--            IF NOT arr_expense[arr_curr()].expense_type MATCHES "miles" THEN
--                LET arr_expense[arr_curr()].total_paid_by_employee =
--                    arr_expense[arr_curr()].amount
--                DISPLAY "Total Paid ",
--                    arr_expense[arr_curr()].total_paid_by_employee
--                --    -- WHENEVER ERROR STOP
--            END IF
--
--    END INPUT
--
--    LET expense_cnt = arr_expense.getLength()
--
--    IF (int_flag) THEN
--        LET int_flag = FALSE
--    END IF
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arr_expense.getLength()

    INPUT ARRAY arr_expense
        WITHOUT DEFAULTS
        FROM sa_rec.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arr_expense[curr_pa].serial_no
                FROM exp_details
            IF arr_expense[curr_pa].serial_no IS NULL
                OR arr_expense[curr_pa].serial_no == 0 THEN
                LET arr_expense[curr_pa].serial_no = 1
            END IF
            LET arr_expense[curr_pa].Exp_ID = exphead_rec.exp_id
            LET arr_expense[curr_pa].amount = 0
            LET arr_expense[curr_pa].date = TODAY

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF

#Validation
        AFTER FIELD expense_type
            IF (arr_expense[arr_curr()].expense_type IS NULL) THEN
                ERROR "this feild cant be null"
                NEXT FIELD expense_type
            END IF
            IF (arr_expense[arr_curr()].expense_type MATCHES "miles") THEN
                NEXT FIELD mileage
            END IF
            NEXT FIELD amount

        AFTER FIELD amount
            IF arr_expense[arr_curr()].expense_type MATCHES "miles" THEN
                LET arr_expense[arr_curr()].amount =
                    0.575 * arr_expense[arr_curr()].mileage
            END IF

        AFTER FIELD charge_to_fourjs
            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            IF arr_expense[arr_curr()].expense_type MATCHES "AIRFARE" THEN
                LET arr_expense[arr_curr()].total_charge_to_four_js =
                    arr_expense[arr_curr()].amount
                        + arr_expense[arr_curr()].charge_to_fourjs
                LET arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                LET arr_expense[arr_curr()].total_charge_to_four_js =
                    arr_expense[arr_curr()].charge_to_fourjs
                DISPLAY "Total Charged ",
                    arr_expense[arr_curr()].total_charge_to_four_js
            END IF
            IF arr_expense[arr_curr()].expense_type MATCHES "AIRFARE" THEN
                LET arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                LET arr_expense[arr_curr()].total_paid_by_employee =
                    arr_expense[arr_curr()].amount

                DISPLAY "Total Paid ",
                    arr_expense[arr_curr()].total_paid_by_employee
            END IF

    END INPUT

    LET expense_cnt = arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

--FUNCTION fill_data(cmb)
--
--    DEFINE cmb ui.ComboBox
--    CALL cmb.addItem(101, "Entertainment")
--    CALL cmb.addItem(102, "Meals")
--    CALL cmb.addItem(103, "Lodging")
--    CALL cmb.addItem(104, "Auto Rent")
--    CALL cmb.addItem(105, "Miles")
--    CALL cmb.addItem(106, "Fuel")
--    CALL cmb.addItem(107, "Transport")
--    CALL cmb.addItem(108, "Tolls")
--    CALL cmb.addItem(109, "Phone")
--    CALL cmb.addItem(110, "Others")
--    CALL cmb.addItem(111, "airfare")
--
--END FUNCTION

FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arr_expense[curr_pa].exp_id = exphead_rec.exp_id
    INSERT INTO exp_details(
        serial_no,
        exp_id,
        dATE,
        location,
        expense_type,
        mileage,
        amount,
        charge_to_fourjs)
        VALUES(arr_expense[curr_pa].serial_no,
            arr_expense[curr_pa].exp_id,
            arr_expense[curr_pa].date,
            arr_expense[curr_pa].location,
            arr_expense[curr_pa].expense_type,
            arr_expense[curr_pa].mileage,
            arr_expense[curr_pa].amount,
            arr_expense[curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = arr_expense[curr_pa].date,
            location = arr_expense[curr_pa].location,
            expense_type = arr_expense[curr_pa].expense_type,
            mileage = arr_expense[curr_pa].mileage,
            amount = arr_expense[curr_pa].amount,
            charge_to_fourjs = arr_expense[curr_pa].charge_to_fourjs
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sb_rec.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            LET arr_expense[curr_pa1].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_id = exphead_rec.exp_id
            --LET arr_expense[curr_pa1].amount = 0
            LET busn[curr_pa1].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD date
            IF opflag = "U" THEN
                NEXT FIELD amount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                --CALL DIALOG.setFieldActive("amount",0)
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arr_expense[g].exp_id
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            date = busn[curr_pa].date,
            amount = busn[curr_pa].amount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION

FUNCTION print_fun()
    DEFINE rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        exp_date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        exp_amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9, 2),
        total_charged_to_fourjs DECIMAL(9, 2),
        total_paid_by_employee DECIMAL(9, 2),

        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
        Entertainment VARCHAR(60),
        Meals VARCHAR(60),
        Lodging VARCHAR(60),
        Autorent VARCHAR(60),
        Fuel VARCHAR(60),
        Transport VARCHAR(60),
        Tolls VARCHAR(60),
        Miles VARCHAR(60),
        Phone VARCHAR(60),
        Other VARCHAR(60),
        airfare VARCHAR(60)

    END RECORD

    IF fgl_report_loadCurrentSettings("reporte.4rp") THEN
        CALL fgl_report_selectDevice("PDF")

        CALL fgl_report_selectPreview(TRUE)
        LET handl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT Report_Emp TO XML HANDLER handl

    DECLARE curs1 CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs
                total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM emp_mast, exp_header, exp_details, business_details
            WHERE exp_details.exp_id = exphead_rec.exp_id
                AND exp_header.exp_id = exphead_rec.exp_id
                AND emp_mast.emp_no = exphead_rec.emp_no
            GROUP BY exp_details.serial_no
    LET total = 0
    LET totalc = 0
    FOREACH curs1 INTO rec.*
        IF rec.expense_type MATCHES "Entertainment" THEN
            LET rec.Entertainment = 0
            --ELSE
            LET rec.Entertainment = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Meals" THEN
            --LET rec.Meals = 0
            -- ELSE
            LET rec.Meals = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Loding" THEN
            -- LET rec.Lodging = 0
            -- ELSE
            LET rec.Lodging = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Auto Rent" THEN
            --LET rec.Autorent = 0
            --ELSE
            LET rec.Autorent = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Fuel" THEN
            --LET rec.fuel = 0
            -- ELSE
            LET rec.fuel = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Transport" THEN
            -- LET rec.Transport = 0
            -- ELSE
            LET rec.Transport = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Tolls" THEN
            -- LET rec.Tolls = 0
            -- ELSE
            LET rec.Tolls = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Miles" THEN
            -- LET rec.Miles = 0
            -- ELSE
            LET rec.miles = rec.mileage
        END IF
        IF rec.expense_type MATCHES "Phone" THEN
            --LET rec.phone = 0
            --ELSE
            LET rec.phone = rec.amount1
        END IF
        IF rec.expense_type MATCHES "Other" THEN
            --LET rec.other=0
            -- ELSE
            LET rec.Other = rec.amount1
        END IF
        IF rec.expense_type MATCHES "airfare" THEN
            -- LET rec.Airfare = 0
            -- ELSE
            LET rec.Airfare = rec.amount1
        END IF

        LET total = total + rec.total_paid_by_employee
        LET totalcharge = totalcharge + rec.total_charged_to_fourjs
        OUTPUT TO REPORT Report_Emp(rec.*)
    END FOREACH

    FINISH REPORT Report_Emp
END FUNCTION

REPORT Report_Emp(myrec)
    DEFINE myrec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9, 2),
        total_charged_to_fourjs DECIMAL(9, 2),
        total_paid_by_employee DECIMAL(9, 2),
        -- total DECIMAL(9,2),
        -- totalc DECIMAL(9,2),
        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,

        Entertainment VARCHAR(60),
        Meals VARCHAR(60),
        Lodging VARCHAR(60),
        Autorent VARCHAR(60),
        Fuel VARCHAR(60),
        Transport VARCHAR(60),
        Tolls VARCHAR(60),
        Miles VARCHAR(60),
        Phone VARCHAR(60),
        Other VARCHAR(60),
        airfare VARCHAR(60)

    END RECORD
    DEFINE total_val DECIMAL(9, 2)

    ORDER EXTERNAL BY myrec.exp_id

    FORMAT
        FIRST PAGE HEADER
            LET totalcharge = 0
            LET myrec.charge_to_fourjs = totalcharge
        PAGE HEADER
            PRINTX today_date
        ON EVERY ROW
            LET total_val =
                myrec.charge_to_fourjs
                    + myrec.total_charged_to_fourjs
                    + myrec.total_paid_by_employee
            DISPLAY "myrec.*", myrec.exp_id
            PRINTX myrec.exp_id,
                myrec.emp_no,
                myrec.notes,
                myrec.charge_type,
                myrec.emp_name,
                myrec.position,
                myrec.manager,
                myrec.department,
                myrec.date,
                myrec.location,
                myrec.expense_type,
                myrec.mileage,
                myrec.amount,
                myrec.charge_to_fourjs,
                myrec.miles_total,
                myrec.total_paid_by_employee,
                myrec.total_charged_to_fourjs,
                myrec.date1,
                myrec.amount1,
                myrec.company_name,
                myrec.business_trans,
                myrec.Entertainment,
                myrec.Meals,
                myrec.Lodging,
                myrec.Autorent,
                myrec.Fuel,
                myrec.Transport,
                myrec.Tolls,
                myrec.Miles,
                myrec.Phone,
                myrec.Other,
                myrec.airfare

--AFTER GROUP OF myrec.emp_name
--PRINTX myrec.emp_name,"form record"
            LET today_date = DATE(CURRENT)
            PRINTX today_date
            -- PRINTX total
            --PRINTX totalc
            PRINTX rpt,
                "-------------------------END REPORT------------------------"

        AFTER GROUP OF myrec.exp_id
            PRINTX myrec.exp_id, "FORM RECORDS"
            PRINTX total
            PRINTX totalcharge
            SKIP 2 LINES

END REPORT
