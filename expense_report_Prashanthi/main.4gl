IMPORT FGL zoom
DATABASE expensedb

GLOBALS 
    DEFINE
    emp_rec RECORD
        exp_id LIKE expense_header.exp_id,
        emp_no LIKE expense_header.emp_no,
        notes LIKE expense_header.notes,
        charge_type LIKE expense_header.charge_type,
       -- emp_no1 LIKE emp_mast.emp_no,
        emp_name LIKE employee_mast.emp_name,
        position LIKE employee_mast.position,
        manager LIKE employee_mast.manager,
        department LIKE employee_mast.department
    END RECORD,
    
    arr_expense DYNAMIC ARRAY OF RECORD
        serial_no LIKE exp_details.serial_no,
        Exp_ID LIKE exp_details.Exp_ID,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2)
    END RECORD
    
    TYPE busi RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        bdate LIKE business_details.bdate,
        bamount LIKE business_details.bamount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans
        
    END RECORD
--DEFINE f_total DECIMAL(9,2)
DEFINE total DECIMAL(16,2)
DEFINE totalc DECIMAL(16,2)
DEFINE busn DYNAMIC ARRAY OF busi
DEFINE today_date DATE
     DEFINE rpt VARCHAR(255)
     DEFINE handl om.SaxDocumentHandler

END GLOBALS 
CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575


MAIN 
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
    --CALL ui.Interface.loadStyles("styles")
    --CALL ui.Interface.loadActionDefaults("actions")
    --CALL ui.Form.setDefaultInitializer("initializeForm")
    CLOSE WINDOW SCREEN
    OPEN WINDOW Emp WITH FORM "mainper"
    MENU
        ON ACTION NEW
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM
         ON ACTION PRINT
        CALL  print_fun()  
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW Emp
END MAIN 

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE emp_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO emp_rec.exp_id FROM expense_header
    IF emp_rec.exp_id IS NULL OR emp_rec.exp_id == 0 THEN
        LET emp_rec.exp_id = 100
    END IF
    
    LET int_flag = FALSE
    INPUT BY NAME emp_rec.exp_id,
        emp_rec.emp_no,
        emp_rec.notes,
        emp_rec.charge_type,
        emp_rec.emp_name,
        emp_rec.position,
        emp_rec.manager,
        emp_rec.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        
        ON CHANGE emp_no
            SELECT * INTO emp_rec.* FROM expense_header WHERE emp_no = emp_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            IF emp_rec.notes IS NULL THEN 
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF 
            IF NOT emp_rec.notes MATCHES "[a-zA-Z]*" THEN 
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF 

        ON ACTION zoom
            CALL display_emplist(
                ) RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET emp_rec.emp_no = id
                LET emp_rec.emp_name = NAME
                LET emp_rec.position = position
                LET emp_rec.manager = manager
                LET emp_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME emp_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO expense_header(
        exp_id,
        emp_no,
        notes,
        charge_type)
        VALUES(emp_rec.exp_id,
            emp_rec.emp_no,
            emp_rec.notes,
            emp_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added-----Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON expense_header.exp_id,
            expense_header.emp_no,
            expense_header.notes,
            expense_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
|| "expense_header.exp_id,"
|| "expense_header.emp_no, "
|| "expense_header.notes, "
|| "expense_header.charge_type, "
|| "employee_mast.emp_name, "
|| "employee_mast.position, "
|| "employee_mast.manager, "
|| "employee_mast.department "
|| "FROM expense_header, employee_mast, exp_details "
|| "WHERE expense_header.exp_id = exp_details.exp_id and employee_mast.emp_no = expense_header.emp_no "
|| "AND "
|| where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO emp_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO emp_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME emp_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9,2),
            total_paid_by_employee DECIMAL(9,2)
        END RECORD

    IF emp_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee
            FROM exp_details
            WHERE exp_details.Exp_ID = emp_rec.exp_id

    LET expense_cnt = 0
    CALL arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            bdate LIKE business_details.bdate,
            bamount LIKE business_details.bamount,
            company_name LIKE business_details.company_name,
            bussiness_trans LIKE business_details.business_trans
            
        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.bdate,
            business_details.bamount,
            business_details.company_name,
            business_details.business_trans
            
            FROM business_details
            WHERE business_details.Exp_ID = emp_rec.exp_id

    LET bussiness_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET busn[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expense_inpupd()
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arr_expense.getLength()

    INPUT ARRAY arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arr_expense[curr_pa].serial_no
                FROM exp_details
            IF arr_expense[curr_pa].serial_no IS NULL
                OR arr_expense[curr_pa].serial_no == 0 THEN
                LET arr_expense[curr_pa].serial_no = 1
            END IF
            LET arr_expense[curr_pa].Exp_ID = emp_rec.exp_id
            LET arr_expense[curr_pa].amount = 0
            LET arr_expense[curr_pa].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD expense_type
            IF (arr_expense[arr_curr()].expense_type = 105) THEN
                MESSAGE "Choose one expense_type"
                CALL DIALOG.setFieldActive("amount", 0)
                NEXT FIELD mileage
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
                
            END IF

            


            
        ON CHANGE mileage
            WHENEVER ERROR CONTINUE
            LET arr_expense[arr_curr()].miles_total =
                C_TAX * arr_expense[arr_curr()].mileage

            WHENEVER ERROR STOP
            IF (SQLCA.SQLCODE = 0) THEN
                DISPLAY BY NAME arr_expense[arr_curr()].*
                --CALL DIALOG.setFieldActive("amount",FALSE)
            END IF
        
        AFTER FIELD charge_to_fourjs
            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            -- WHENEVER ERROR CONTINUE
            LET arr_expense[arr_curr()].total_charged_to_fourjs =
                arr_expense[arr_curr()].amount
                    + arr_expense[arr_curr()].charge_to_fourjs
            DISPLAY "Total Charged ",
                arr_expense[arr_curr()].total_charged_to_fourjs

            -- AFTER ROW
            LET arr_expense[arr_curr()].total_paid_by_employee =
                arr_expense[arr_curr()].miles_total
            IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
                LET arr_expense[arr_curr()].total_paid_by_employee =
                    arr_expense[arr_curr()].amount
                DISPLAY "Total Paid ",
                    arr_expense[arr_curr()].total_paid_by_employee
                -- WHENEVER ERROR STOP
            END IF

    END INPUT

    LET expense_cnt = arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

{FUNCTION fill_data(cmb)

    DEFINE cmb ui.ComboBox
    CALL cmb.addItem(101, "Entertainment")
    CALL cmb.addItem(102, "Meals")
    CALL cmb.addItem(103, "Lodging")
    CALL cmb.addItem(104, "Auto Rent")
    CALL cmb.addItem(105, "Miles")
    CALL cmb.addItem(106, "Fuel")
    CALL cmb.addItem(107, "Transport")
    CALL cmb.addItem(108, "Tolls/PK")
    CALL cmb.addItem(109, "Phone")
    CALL cmb.addItem(110, "Others")
    CALL cmb.addItem(111, "Airfare")

END FUNCTION
}
FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arr_expense[curr_pa].Exp_ID = emp_rec.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(arr_expense[curr_pa].serial_no,
            arr_expense[curr_pa].Exp_ID,
            arr_expense[curr_pa].date,
            arr_expense[curr_pa].location,
            arr_expense[curr_pa].expense_type,
            arr_expense[curr_pa].mileage,
            arr_expense[curr_pa].amount,
            arr_expense[curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = arr_expense[curr_pa].date,
            location = arr_expense[curr_pa].location,
            expense_type = arr_expense[curr_pa].expense_type,
            mileage = arr_expense[curr_pa].mileage,
            amount = arr_expense[curr_pa].amount,
            charge_to_fourjs = arr_expense[curr_pa].charge_to_fourjs
        
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET arr_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_ID = emp_rec.exp_id
            --LET arr_expense[curr_pa1].amount = 0
            LET busn[curr_pa1].bdate = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD bdate
            IF opflag = "U" THEN
                NEXT FIELD bamount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                --CALL DIALOG.setFieldActive("amount",0)
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arr_expense[g].exp_id
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            bdate = busn[curr_pa].bdate,
            bamount = busn[curr_pa].bamount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION

FUNCTION print_fun()
     DEFINE rec RECORD 
        exp_id LIKE expense_header.exp_id,
        emp_no LIKE expense_header.emp_no,
         notes LIKE      expense_header.notes,
        charge_type LIKE expense_header.charge_type,
        --emp_no LIKE     employee_mast.emp_no,
        emp_name LIKE   employee_mast.emp_name,
        position LIKE   employee_mast.position,
        manager LIKE    employee_mast.manager,
        department LIKE employee_mast.department,
        DATE  LIKE   exp_details.date,
        location LIKE    exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE   exp_details.mileage,
        amount LIKE   exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),
        bdate LIKE business_details.bdate,
        bamount LIKE business_details.bamount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
         Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Auto_Rent VARCHAR(50),
        Miles VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)
        --
        END RECORD 

        IF fgl_report_loadCurrentSettings("Reportexp.4rp") THEN 
            CALL fgl_report_selectDevice("PDF")
            CALL fgl_report_selectPreview(TRUE)
            LET handl = fgl_report_commitCurrentSettings()
        ELSE 
            EXIT PROGRAM 
        END IF 

            START REPORT Report_Emp
                TO XML HANDLER handl

            DECLARE curs123 CURSOR FOR 
            SELECT expense_header.exp_id,
                   expense_header.emp_no,
                  expense_header.notes,
                  expense_header.charge_type,
                   --employee_mast.emp_no,         
                   employee_mast.emp_name,
                   employee_mast.position,
                   employee_mast.manager,
                   employee_mast.department,
                  exp_details.date,
                  exp_details.location,
                  exp_details.expense_type,
                  exp_details.mileage,
                  exp_details.amount,
                  exp_details.charge_to_fourjs,
                   exp_details.mileage * C_TAX miles_total ,
                   exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs ,
                   exp_details.amount total_paid_by_employee,
                    business_details.bdate,
                   business_details.bamount,
                   business_details.company_name,
                   business_details.business_trans
                 FROM expense_header,employee_mast,exp_details,business_details
                WHERE exp_details.exp_id = emp_rec.exp_id
                AND expense_header.exp_id = emp_rec.exp_id AND employee_mast.emp_no = emp_rec.emp_no
                  GROUP BY exp_details.serial_no
                  
               
                LET total = 0
                LET totalc = 0 
            FOREACH curs123 INTO rec.*
            
        IF rec.expense_type = 101 THEN
       LET rec.expense_type = "Entertainment" 
        LET rec.Entertainment = rec.amount
        END IF 
        IF rec.expense_type = 102 THEN
        LET rec.expense_type = "Meals"  
        LET rec.Meals = rec.amount
        END IF 
        IF rec.expense_type =103 THEN
        LET rec.expense_type = "Lodging" 
        LET rec.Lodging = rec.amount 
        END IF 
        IF rec.expense_type = 104 THEN
        LET rec.expense_type = "Auto_Rent" 
        LET rec.Auto_Rent = rec.amount 
        END IF 
        IF rec.expense_type = 105 THEN
        LET rec.expense_type = "Miles" 
        LET rec.Miles = rec.amount 
        END IF
        IF rec.expense_type = 106 THEN
        LET rec.expense_type = "Fuel" 
        LET rec.Fuel = rec.amount 
        END IF
        IF rec.expense_type = 107 THEN
        LET rec.expense_type = "Transport" 
        LET rec.Transport = rec.amount
        END IF
        IF rec.expense_type = 108 THEN
        LET rec.expense_type = "Tolls" 
        LET rec.Tolls = rec.amount 
        END IF
        IF rec.expense_type = 109 THEN
        LET rec.expense_type = "phone" 
        LET rec.Phone = rec.amount 
        END IF
        IF rec.expense_type = 110 THEN
        LET rec.expense_type = "Others" 
        LET rec.Others = rec.amount 
        END IF
         IF rec.expense_type = 111 THEN
        LET rec.expense_type = "Airfare" 
        LET rec.Airfare = rec.amount 
        END IF
         --LET f_total = f_total  + rec.total_charged_to_fourjs
         LET total = total + rec.total_paid_by_employee
        LET totalc = totalc + rec.total_charged_to_fourjs

        OUTPUT TO REPORT Report_Emp(rec.*)
    END FOREACH

        FINISH REPORT Report_Emp
END FUNCTION 

REPORT Report_Emp(my)
    DEFINE my RECORD 
        exp_id LIKE expense_header.exp_id,
        emp_no LIKE expense_header.emp_no,
         notes LIKE expense_header.notes,
        charge_type LIKE expense_header.charge_type,
        --emp_no LIKE employee_mast.emp_no,
        emp_name LIKE employee_mast.emp_name,
        position LIKE employee_mast.position,
        manager LIKE employee_mast.manager,
        department LIKE employee_mast.department,
        --expense_id1 LIKE expense_details.expense_id,
         date LIKE      exp_details.date,
        location LIKE       exp_details.location,
        expense_type LIKE   exp_details.expense_type,
        mileage LIKE        exp_details.mileage,
        amount LIKE         exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),
        bdate LIKE business_details.bdate,
        bamount LIKE business_details.bamount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
         Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Auto_Rent VARCHAR(50),
        Miles VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)

        END RECORD 

         ORDER EXTERNAL BY my.exp_id

         FORMAT 
           FIRST PAGE HEADER 
           PAGE HEADER 
           
              PRINTX today_date
            ON EVERY ROW 
            LET today_date = DATE(today)
              LET my.date = today_date
                DISPLAY "Today Date,", today_date
                PRINTX today_date
               --DISPLAY "myrec.*",myrec.expense_id
               PRINTX 
                       my.emp_no,
                       my.emp_name,
                       my.position,
                       my.manager,
                       my.department,
                       my.notes,
                       my.charge_type,
                       my.date,
                       my.location,
                       my.expense_type,
                       my.mileage,
                       my.amount,
                       my.charge_to_fourjs,
                       my.miles_total,
                       my.total_charged_to_fourjs,
                       my.total_paid_by_employee,
                       my.bdate,
                       my.bamount,
                       my.company_name,
                       my.business_trans
                       
                       PRINTX rpt,"-------------------------END REPORT------------------------"

                       AFTER GROUP OF my.exp_id
                          PRINTX my.exp_id, "FORM RECORDS"
                          PRINTX total
                          PRINTX totalc
                          SKIP 2 LINES
    
END REPORT 
