SCHEMA expe

FUNCTION display_emplist()
    DEFINE
        fr_emp_arr DYNAMIC ARRAY OF RECORD
            emp_no LIKE Emp_mast.emp_no,
            emp_name LIKE Emp_mast.emp_name,
            position LIKE Emp_mast.position,
            manager LIKE Emp_mast.manager,
            department LIKE Emp_mast.department
        END RECORD,
        fr_emp_rec1 RECORD
            emp_no LIKE Emp_mast.emp_no,
            emp_name LIKE Emp_mast.emp_name,
            position LIKE Emp_mast.position,

            manager LIKE Emp_mast.manager,
            department LIKE Emp_mast.department
        END RECORD,
        re_num LIKE Emp_mast.emp_no,
        re_name LIKE Emp_mast.emp_name,
        re_position LIKE Emp_mast.position,
        re_manager LIKE Emp_mast.manager,
        re_department LIKE Emp_mast.department,
        f_curr_pa, f_idx SMALLINT

    OPEN WINDOW EmpFo WITH FORM "zoom"

    DECLARE emp_curs CURSOR FOR
        SELECT emp_no, emp_name, position, manager, department
            FROM Emp_mast
            ORDER BY emp_no

    LET f_idx = 0
    CALL fr_emp_arr.clear()
    FOREACH emp_curs INTO fr_emp_rec1.*
        LET f_idx = f_idx + 1
        LET fr_emp_arr[f_idx].* = fr_emp_rec1.*
    END FOREACH

    LET re_num = 0
    LET re_name = NULL

    IF f_idx > 0 THEN
        LET int_flag = FALSE
        DISPLAY ARRAY fr_emp_arr TO sa_cust.* ATTRIBUTES(COUNT = f_idx)
        IF (NOT int_flag) THEN
            LET f_curr_pa = arr_curr()
            LET re_num = fr_emp_arr[f_curr_pa].emp_no
            LET re_name = fr_emp_arr[f_curr_pa].emp_name
            LET re_position = fr_emp_arr[f_curr_pa].position
            LET re_manager = fr_emp_arr[f_curr_pa].manager
            LET re_department = fr_emp_arr[f_curr_pa].department
        END IF
    END IF

    CLOSE WINDOW EmpFo

    RETURN re_num, re_name, re_position, re_manager, re_department

END FUNCTION  
