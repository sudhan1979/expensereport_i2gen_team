--main.4gl

IMPORT util  
IMPORT FGL zoom
DATABASE expe

GLOBALS 
    DEFINE
    gr_emp_rec RECORD                         ##record having the same structure as the exp_header table.##
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department
    END RECORD,
    gr_arr_expense DYNAMIC ARRAY OF RECORD     ## Dynamic array having the same structure as the exp_details table. ##
        serial_no LIKE exp_details.serial_no,
        Exp_ID LIKE exp_details.Exp_ID,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2)
    END RECORD
    TYPE gr_busi RECORD                          ##record having the same structure as the business_details table.##
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        date LIKE business_details.date,
        amount LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans
        
    END RECORD
DEFINE f_total DECIMAL(9,2)             ## defines the global variable f_total, to hold the grand total value.##
DEFINE gr_busn DYNAMIC ARRAY OF gr_busi
DEFINE today_date DATE
DEFINE rpt VARCHAR(255)
DEFINE handl om.SaxDocumentHandler
END GLOBALS 

## constant messages ##

CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575


## start of the MAIN program block ##

MAIN 
    DEFINE m_employee, m_query_ok SMALLINT
    DEFER INTERRUPT
    
## closes the default window named SCREEN. ##
    CLOSE WINDOW SCREEN 
    
## WITH FORM syntax to open a window having the identifier W1 containing the form identified as main. ##
    OPEN WINDOW W1 WITH FORM "main" 

## MENU statement. ##    
    MENU
        ON ACTION NEW
            CLEAR FORM
            LET m_query_ok = FALSE
            CALL close_emp()
            LET m_employee = emp_insert()
            IF m_employee THEN
                CALL gr_arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET m_query_ok = emp_query()
            LET m_employee = m_query_ok
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM
        ON ACTION print 
          CALL print_fun()
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW W1          ## The window W1 is closed which automatically closes the form. ##
END MAIN                     ##The program disconnects from the database; as there are no more statements in MAIN, the program terminates. ##


FUNCTION emp_insert()
    DEFINE
        f_id INTEGER,
        f_name STRING,
        f_position STRING,
        f_manager STRING,
        f_department STRING
    MESSAGE msg11

    INITIALIZE gr_emp_rec.* TO NULL             ## record values are initialized to NULL prior to calling the INPUT statement ##
    SELECT MAX(exp_id) + 1 INTO gr_emp_rec.exp_id FROM exp_header
    IF gr_emp_rec.exp_id IS NULL OR gr_emp_rec.exp_id == 0 THEN
        LET gr_emp_rec.exp_id = 100
    END IF
    
    LET int_flag = FALSE                        ## sets the INT_FLAG global variable to FALSE prior to the INPUT statement ##
    INPUT BY NAME gr_emp_rec.exp_id,
        gr_emp_rec.emp_no,
        gr_emp_rec.notes,
        gr_emp_rec.charge_type,
        gr_emp_rec.emp_name,
        gr_emp_rec.position,
        gr_emp_rec.manager,
        gr_emp_rec.department
        WITHOUT DEFAULTS                
        ATTRIBUTES(UNBUFFERED)              ## The UNBUFFERED and WITHOUT DEFAULTS clauses of the INPUT statement are used. ##

        
        ON CHANGE emp_no
            SELECT * INTO gr_emp_rec.* FROM exp_header WHERE emp_no = gr_emp_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD notes
            END IF

        AFTER FIELD notes
            IF gr_emp_rec.notes IS NULL THEN 
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF 
            IF NOT gr_emp_rec.notes MATCHES "[a-zA-Z]*" THEN 
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF 

        ON ACTION zoom
            CALL display_emplist(
                ) RETURNING f_id, f_NAME, f_position, f_manager, f_department

            IF (f_id > 0) THEN
                LET gr_emp_rec.emp_no = f_id
                LET gr_emp_rec.emp_name = f_NAME
                LET gr_emp_rec.position = f_position
                LET gr_emp_rec.manager = f_manager
                LET gr_emp_rec.department = f_department

            END IF

    END INPUT

    IF (int_flag) THEN                  ## The INT_FLAG is checked to see if the user has canceled the input.##
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION



FUNCTION employee_insert()

    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id,
        emp_no,
        notes,
        charge_type)
        VALUES(gr_emp_rec.exp_id,
            gr_emp_rec.emp_no,
            gr_emp_rec.notes,
            gr_emp_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added, Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

FUNCTION emp_query()
    DEFINE f_where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME f_where_clause
        ON exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(f_where_clause)

END FUNCTION

FUNCTION employee_select(f_where_clause)
    DEFINE
        f_where_clause STRING,
        f_sql_text STRING

    LET f_sql_text =
        "SELECT "
            || "exp_header.exp_id,"
            || "exp_header.emp_no, "
            || "exp_header.notes,  "
            || "exp_header.charge_type, "
            || "emp_mast.emp_name, "
            || "emp_mast.position, "
            || "emp_mast.manager, "
            || "emp_mast.department "
            || "FROM exp_header, emp_mast, exp_details "
            || "WHERE exp_header.exp_id = exp_details.exp_id "
            || "AND "
            || f_where_clause

    DECLARE emp_curs SCROLL CURSOR FROM f_sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(f_fetch_flag)
    DEFINE f_fetch_flag SMALLINT

    IF f_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO gr_emp_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO gr_emp_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME gr_emp_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(f_fetch_flag)
    DEFINE f_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(f_fetch_flag)) THEN
        IF (f_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        f_expense_cnt INTEGER,
        fr_exp_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9,2),
            total_paid_by_employee DECIMAL(9,2)
        END RECORD

    IF gr_emp_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            --exp_details.amount total_paid_by_employee
            exp_details.mileage * C_TAX total_paid_by_employee,
            exp_details.mileage * C_TAX total_charged_to_fourjs
            FROM exp_details
            WHERE exp_details.Exp_ID = gr_emp_rec.exp_id

    LET f_expense_cnt = 0
    CALL gr_arr_expense.clear()
    FOREACH exp_curs INTO fr_exp_rec.*
        LET f_expense_cnt = f_expense_cnt + 1
        LET gr_arr_expense[f_expense_cnt].* = fr_exp_rec.*
    END FOREACH
    FREE exp_curs

    CALL exp_display()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        f_bussiness_cnt INTEGER,
        fr_bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            date LIKE business_details.date,
            amount LIKE business_details.amount,
            company_name LIKE business_details.company_name,
            bussiness_trans LIKE business_details.business_trans
            
        END RECORD

    IF fr_bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            
            FROM business_details
            WHERE business_details.Exp_ID = gr_emp_rec.exp_id

    LET f_bussiness_cnt = 0
    CALL gr_busn.clear()
    FOREACH buss_curs INTO fr_bus_rec.*
        LET f_bussiness_cnt = f_bussiness_cnt + 1
        LET gr_busn[f_bussiness_cnt].* = fr_bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY gr_busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION exp_display()
    DISPLAY ARRAY gr_arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expense_inpupd()
    DEFINE
        f_opflag CHAR(1),
        f_expense_cnt, f_curr_pa SMALLINT

    LET f_opflag = "U"

    LET f_expense_cnt = gr_arr_expense.getLength()

    INPUT ARRAY gr_arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET f_curr_pa = ARR_CURR()
            LET gr_arr_expense[f_curr_pa].mileage = 0.575
            LET f_opflag = "U"

        BEFORE INSERT
            LET f_opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO gr_arr_expense[f_curr_pa].serial_no
                FROM exp_details
            IF gr_arr_expense[f_curr_pa].serial_no IS NULL
                OR gr_arr_expense[f_curr_pa].serial_no == 0 THEN
                LET gr_arr_expense[f_curr_pa].serial_no = 1
            END IF
            LET gr_arr_expense[f_curr_pa].Exp_ID = gr_emp_rec.exp_id
            LET gr_arr_expense[f_curr_pa].amount = 0
            LET gr_arr_expense[f_curr_pa].date = TODAY
          

        AFTER INSERT
            CALL expense_insert(f_curr_pa)

        BEFORE DELETE
            CALL expense_delete(f_curr_pa)

        ON ROW CHANGE
            CALL expense_update(f_curr_pa)

        BEFORE FIELD Exp_ID
            IF f_opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD location 
              IF gr_arr_expense[arr_curr()].location IS NULL THEN 
                MESSAGE "Enter Location"
                NEXT FIELD location 
              END IF 
              IF NOT gr_arr_expense[arr_curr()].location MATCHES "[a-zA-Z]*" THEN 
                MESSAGE "Enter Location Correctly"
                NEXT FIELD location
            END IF 
        AFTER FIELD expense_type
            IF (gr_arr_expense[arr_curr()].expense_type = 105) THEN
                MESSAGE "Choose one expense_type"
                CALL DIALOG.setFieldActive("amount", 0)
                
                LET gr_arr_expense[arr_curr()].charge_to_fourjs = 0
                NEXT FIELD mileage
                LET gr_arr_expense[arr_curr()].total_paid_by_employee = gr_arr_expense[arr_curr()].miles_total
            ELSE 
                CALL dialog.setFieldActive("amount",1)
                NEXT FIELD amount
               
            END IF 
            IF (gr_arr_expense[arr_curr()].expense_type = 111)  THEN 
                CALL DIALOG.setFieldActive("mileage",0)
                NEXT FIELD AMOUNT
                LET gr_arr_expense[arr_curr()].total_charged_to_fourjs = gr_arr_expense[arr_curr()].amount + gr_arr_expense[arr_curr()].charge_to_fourjs
            LET gr_arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
            END IF 
         


        ON CHANGE mileage
            WHENEVER ERROR CONTINUE
            LET gr_arr_expense[arr_curr()].miles_total =
                C_TAX * gr_arr_expense[arr_curr()].mileage

            WHENEVER ERROR STOP
            IF (SQLCA.SQLCODE = 0) THEN
                DISPLAY BY NAME gr_arr_expense[arr_curr()].*
                
            END IF
            
        AFTER FIELD charge_to_fourjs
            IF gr_arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            
            LET gr_arr_expense[arr_curr()].total_charged_to_fourjs =
                gr_arr_expense[arr_curr()].amount
                    + gr_arr_expense[arr_curr()].charge_to_fourjs
            DISPLAY "Total Charged ",
                gr_arr_expense[arr_curr()].total_charged_to_fourjs

            LET gr_arr_expense[arr_curr()].total_paid_by_employee =
                gr_arr_expense[arr_curr()].miles_total
            
            IF NOT gr_arr_expense[arr_curr()].expense_type = 105 THEN
                LET gr_arr_expense[arr_curr()].total_paid_by_employee =
                    gr_arr_expense[arr_curr()].amount
                DISPLAY "Total Paid ",
                    gr_arr_expense[arr_curr()].total_paid_by_employee
                
            END IF
            IF gr_arr_expense[arr_curr()].expense_type = 105 THEN
            LET gr_arr_expense[arr_curr()].total_paid_by_employee = gr_arr_expense[arr_curr()].miles_total
            LET gr_arr_expense[arr_curr()].total_charged_to_fourjs =
            gr_arr_expense[arr_curr()].miles_total
            END IF

    END INPUT

    LET f_expense_cnt = gr_arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION


FUNCTION fill_data(cmb)

    DEFINE cmb ui.ComboBox
    CALL cmb.addItem(101, "Entertainment")
    CALL cmb.addItem(102, "Meals")
    CALL cmb.addItem(103, "Lodging")
    CALL cmb.addItem(104, "Auto Rent")
    CALL cmb.addItem(105, "Miles")
    CALL cmb.addItem(106, "Fuel")
    CALL cmb.addItem(107, "Transport")
    CALL cmb.addItem(108, "Tolls/PK")
    CALL cmb.addItem(109, "Phone")
    CALL cmb.addItem(110, "Others")
    CALL cmb.addItem(111, "Airfare")

END FUNCTION

FUNCTION expense_insert(f_curr_pa)
    DEFINE f_curr_pa SMALLINT

    LET f_curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET gr_arr_expense[f_curr_pa].Exp_ID = gr_emp_rec.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(gr_arr_expense[f_curr_pa].serial_no,
            gr_arr_expense[f_curr_pa].Exp_ID,
            gr_arr_expense[f_curr_pa].date,
            gr_arr_expense[f_curr_pa].location,
            gr_arr_expense[f_curr_pa].expense_type,
            gr_arr_expense[f_curr_pa].mileage,
            gr_arr_expense[f_curr_pa].amount,
            gr_arr_expense[f_curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(f_curr_pa)
    DEFINE f_curr_pa SMALLINT
    LET f_curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = gr_arr_expense[f_curr_pa].date,
            location = gr_arr_expense[f_curr_pa].location,
            expense_type = gr_arr_expense[f_curr_pa].expense_type,
            mileage = gr_arr_expense[f_curr_pa].mileage,
            amount = gr_arr_expense[f_curr_pa].amount,
            charge_to_fourjs = gr_arr_expense[f_curr_pa].charge_to_fourjs
        
        WHERE exp_details.serial_no = gr_arr_expense[f_curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(f_curr_pa)
    DEFINE f_curr_pa SMALLINT
    LET f_curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = gr_arr_expense[f_curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        f_opflag CHAR(1),
        f_buss_cnt, f_curr_pa1 SMALLINT

    LET f_opflag = "U"

    LET f_buss_cnt = gr_busn.getLength()

    INPUT ARRAY gr_busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET f_curr_pa1 = ARR_CURR()
        
            LET f_opflag = "U"

        BEFORE INSERT
            LET f_opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO gr_busn[f_curr_pa1].serial_no
                FROM business_details
            IF gr_busn[f_curr_pa1].serial_no IS NULL
                OR gr_busn[f_curr_pa1].serial_no == 0 THEN
                LET gr_busn[f_curr_pa1].serial_no = 1
            END IF
            LET gr_busn[f_curr_pa1].exp_ID = gr_emp_rec.exp_id
        
            LET gr_busn[f_curr_pa1].date = TODAY

        AFTER INSERT
            CALL insert_buss(f_curr_pa1)

        BEFORE DELETE
            CALL buss_delete(f_curr_pa1)

        ON ROW CHANGE
            CALL buss_update(f_curr_pa1)

        BEFORE FIELD date
            IF f_opflag = "U" THEN
                NEXT FIELD amount
            END IF
        AFTER FIELD company_name
            IF gr_busn[f_curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                NEXT FIELD company_name

            END IF

    END INPUT

    LET f_buss_cnt = gr_busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION insert_buss(f_i)
    DEFINE f_i SMALLINT
    LET f_i = arr_curr()
    LET gr_busn[f_i].exp_id = gr_arr_expense[f_i].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(gr_busn[f_i].*)

    WHENEVER ERROR STOP
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = gr_busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(f_curr_pa)
    DEFINE f_curr_pa SMALLINT
    LET f_curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = gr_busn[f_curr_pa].serial_no,
            date = gr_busn[f_curr_pa].date,
            amount = gr_busn[f_curr_pa].amount,
            company_name = gr_busn[f_curr_pa].company_name,
            business_trans = gr_busn[f_curr_pa].business_trans,
            exp_id = gr_busn[f_curr_pa].exp_id
        WHERE business_details.serial_no = gr_busn[f_curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE f_i INT
    LET f_i = 1
    DISPLAY BY NAME gr_busn[f_i].*
END FUNCTION

FUNCTION print_fun()
     DEFINE f_rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,

        Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Auto_Rent VARCHAR(50),
        Miles VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)

    END RECORD
    

    IF fgl_report_loadCurrentSettings("reporte.4rp") THEN
        CALL fgl_report_selectDevice("PDF")

        CALL fgl_report_selectPreview(TRUE)
        LET handl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT Report_Emp TO XML HANDLER handl

    DECLARE curs123 CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_header.notes,
            exp_header.charge_type,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM exp_header, emp_mast, exp_details, business_details
            WHERE exp_details.exp_id = gr_emp_rec.exp_id
                AND exp_header.exp_id = gr_emp_rec.exp_id AND emp_mast.emp_no = gr_emp_rec.emp_no
            GROUP BY exp_details.serial_no
    LET f_total = 0
    FOREACH curs123 INTO f_rec.*
        
        
        IF f_rec.expense_type = 101 THEN 
        LET f_rec.Entertainment = f_rec.amount
        END IF 
        IF f_rec.expense_type = 102 THEN 
        LET f_rec.Meals = f_rec.amount
        END IF 
        IF f_rec.expense_type = 103 THEN
        LET f_rec.Lodging = f_rec.amount 
        END IF 
        IF f_rec.expense_type = 104 THEN
        LET f_rec.Auto_Rent = f_rec.amount 
        END IF 
        IF f_rec.expense_type = 105 THEN
        LET f_rec.Miles = f_rec.amount 
        --LET f_rec.total_paid_by_employee = f_rec.miles_total
        END IF
        IF f_rec.expense_type = 106 THEN
        LET f_rec.Fuel = f_rec.amount 
        END IF
        IF f_rec.expense_type = 107 THEN
        LET f_rec.Transport = f_rec.amount
        END IF
        IF f_rec.expense_type = 108 THEN
        LET f_rec.Tolls = f_rec.amount 
        END IF
        IF f_rec.expense_type = 109 THEN
        LET f_rec.Phone = f_rec.amount 
        END IF
        IF f_rec.expense_type = 110 THEN
        LET f_rec.Others = f_rec.amount 
        END IF
         IF f_rec.expense_type = 111 THEN
        LET f_rec.Airfare = f_rec.amount 
        --LET f_rec.total_paid_by_employee = 0
        END IF
         LET f_total = f_total  + f_rec.total_charged_to_fourjs

        OUTPUT TO REPORT Report_Emp(f_rec.*)
    END FOREACH

    FINISH REPORT Report_Emp
END FUNCTION 

REPORT Report_Emp(m)
    DEFINE m RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
        Entertainment VARCHAR(50),
        Meals VARCHAR(50),
        Lodging VARCHAR(50),
        Auto_Rent VARCHAR(50),
        Miles VARCHAR(50),
        Fuel VARCHAR(50),
        Transport VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Others VARCHAR(50),
        Airfare VARCHAR(50)

    END RECORD
    
    ORDER EXTERNAL BY m.exp_id
    
    FORMAT
        FIRST PAGE HEADER
        PAGE HEADER 
            PRINTX today_date
        ON EVERY ROW
            LET today_date = DATE(TODAY)
            LET m.date = today_date
            DISPLAY "Today Date,", today_date
            PRINTX today_date
            LET today_date = DATE(TODAY)
            LET m.date1 = today_date
            PRINTX m.emp_no,
                m.emp_name,
                m.position,
                m.manager,
                m.department,
                m.notes,
                m.charge_type,
                m.date,
                m.location,
                m.expense_type,
                m.mileage,
                m.amount,
                m.charge_to_fourjs,
                m.miles_total,
                m.total_charged_to_fourjs,
                m.total_paid_by_employee,
                m.date,
                m.amount1,
                m.company_name,
                m.business_trans,
                m.Entertainment,
                m.Meals,
                m.Lodging,
                m.Auto_Rent,
                m.Miles,
                m.Fuel,
                m.Transport,
                m.Tolls,
                m.Phone,
                m.Others,
                m.Airfare

            PRINTX rpt,
            
            
                "-------------------------END REPORT------------------------"
        AFTER GROUP OF m.exp_id
            PRINTX m.exp_id, "FORM RECORDS"
            PRINTX f_total
            SKIP 2 LINES

END REPORT  


