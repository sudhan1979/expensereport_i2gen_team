IMPORT util 
IMPORT FGL expense_zoom
DATABASE expensedb

GLOBALS 
    DEFINE arr_index INTEGER
    DEFINE TODAYDATE DATE
    DEFINE rpt VARCHAR(200)
    DEFINE prt VARCHAR(200)
    DEFINE handl om.SaxDocumentHandler
    DEFINE cmb ui.ComboBox
    DEFINE list DYNAMIC ARRAY OF INTEGER
    DEFINE cnt INTEGER
    DEFINE d ui.Dialog
    DEFINE answer STRING

    DEFINE
    employee_rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        --emp_no1 LIKE emp_mast.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department
    END RECORD,
    arrs_expense DYNAMIC ARRAY OF RECORD
        serial_no LIKE exp_details.serial_no,
        Exp_ID LIKE exp_details.Exp_ID,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2)
    END RECORD
    TYPE busi RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans
        
    END RECORD
DEFINE busn DYNAMIC ARRAY OF busi
DEFINE total DECIMAL(8,2)
DEFINE totalc DECIMAL(8,2)
END GLOBALS 
CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT CAL_TAX = 0.575


MAIN 
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
    --CALL ui.Interface.loadStyles("styles")
    --CALL ui.Interface.loadActionDefaults("actions")
    --CALL ui.Form.setDefaultInitializer("initializeForm")
    CALL ui.Interface.loadToolBar("expense")
    CLOSE WINDOW SCREEN
    OPEN WINDOW Expense WITH FORM "main"
    MENU
        ON ACTION NEW
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL arrs_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
        ON ACTION PRINT 
        CALL print_fun()
        
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW Expense
END MAIN 

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE employee_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO employee_rec.exp_id FROM exp_header
    IF employee_rec.exp_id IS NULL OR employee_rec.exp_id == 0 THEN
        LET employee_rec.exp_id = 100
    END IF

    
    LET int_flag = FALSE
    INPUT BY NAME employee_rec.exp_id,
        employee_rec.emp_no,
        employee_rec.notes,
        employee_rec.charge_type,
        employee_rec.emp_name,
        employee_rec.position,
        employee_rec.manager,
        employee_rec.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        
        ON CHANGE emp_no
            SELECT * INTO employee_rec.* FROM exp_header WHERE emp_no = employee_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            

        ON ACTION zoom
            CALL display_employeelist(
                ) RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET employee_rec.emp_no = id
                LET employee_rec.emp_name = NAME
                LET employee_rec.position = position
                LET employee_rec.manager = manager
                LET employee_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME employee_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id,
        emp_no,
        notes,
        charge_type)
        VALUES(employee_rec.exp_id,
            employee_rec.emp_no,
            employee_rec.notes,
            employee_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Details  Added"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON exp_header.exp_id
           

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
            || "exp_header.exp_id,"
            || "exp_header.emp_no, "
            || "exp_header.notes,  "
            || "exp_header.charge_type, "
            || "emp_mast.emp_name, "
            || "emp_mast.position, "
            || "emp_mast.manager, "
            || "emp_mast.department "
            || "FROM exp_header, emp_mast, exp_details "
            || "WHERE exp_header.exp_id = exp_details.exp_id AND emp_mast.emp_no = exp_header.emp_no "
            || "AND "
            || where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO employee_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO employee_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME employee_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9,2),
            total_paid_by_employee DECIMAL(9,2)
        END RECORD

    IF employee_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * CAL_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            --exp_details.amount total_paid_by_employee
            exp_details.mileage * CAL_TAX total_paid_by_employee,
            exp_details.mileage * CAL_TAX total_charged_to_fourjs
            FROM exp_details
            WHERE exp_details.Exp_ID = employee_rec.exp_id

    LET expense_cnt = 0
    CALL arrs_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arrs_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            business_date LIKE business_details.business_date,
            business_amount LIKE business_details.business_amount,
            company_name LIKE business_details.company_name,
            business_trans LIKE business_details.business_trans
            
        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
        business_details.exp_id,
            business_details.business_date,
            business_details.business_amount,
            business_details.company_name,
            business_details.business_trans
            
            FROM business_details
            WHERE business_details.exp_id = employee_rec.exp_id

    LET bussiness_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET busn[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY arrs_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expense_inpupd()
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arrs_expense.getLength()

    INPUT ARRAY arrs_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arrs_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arrs_expense[curr_pa].serial_no
                FROM exp_details
            IF arrs_expense[curr_pa].serial_no IS NULL
                OR arrs_expense[curr_pa].serial_no == 0 THEN
                LET arrs_expense[curr_pa].serial_no = 1
            END IF
            LET arrs_expense[curr_pa].Exp_ID = employee_rec.exp_id
            LET arrs_expense[curr_pa].amount = 0
            LET arrs_expense[curr_pa].date = TODAY
            -- LET arrs_expense[curr_pa].total_by_fourjs = 0
            --      LET CAL_TAX = 0.575

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD expense_type
            IF (arrs_expense[arr_curr()].expense_type = "Miles") THEN
                MESSAGE "Choose one expense_type"
                CALL DIALOG.setFieldActive("amount", 0)
                NEXT FIELD mileage
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
                
            END IF
            IF (arrs_expense[arr_curr()].expense_type = "Airfare")  THEN 
                CALL DIALOG.setFieldActive("mileage",0)
                NEXT FIELD amount
                LET arrs_expense[arr_curr()].total_charged_to_fourjs = arrs_expense[arr_curr()].amount + arrs_expense[arr_curr()].charge_to_fourjs
                LET arrs_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                CALL DIALOG.setFieldActive("amount", 1)
                NEXT FIELD amount
            END IF 
            
        ON CHANGE mileage
            WHENEVER ERROR CONTINUE
            LET arrs_expense[arr_curr()].miles_total =
                CAL_TAX * arrs_expense[arr_curr()].mileage

            WHENEVER ERROR STOP
            IF (SQLCA.SQLCODE = 0) THEN
                DISPLAY BY NAME arrs_expense[arr_curr()].*
                
            END IF
            
        AFTER FIELD charge_to_fourjs
            IF arrs_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            -- WHENEVER ERROR CONTINUE
            LET arrs_expense[arr_curr()].total_charged_to_fourjs =
                arrs_expense[arr_curr()].amount
                    + arrs_expense[arr_curr()].charge_to_fourjs
            DISPLAY "Total Charged ",
                arrs_expense[arr_curr()].total_charged_to_fourjs

            -- AFTER ROW
            LET arrs_expense[arr_curr()].total_paid_by_employee =
                arrs_expense[arr_curr()].miles_total
            -- AFTER INPUT --  DISPLAY "Total Charged ",recod[arr_curr()].total_charged_to_fourjs
            IF NOT arrs_expense[arr_curr()].expense_type = "Miles" THEN
                LET arrs_expense[arr_curr()].total_paid_by_employee =
                    arrs_expense[arr_curr()].amount
                DISPLAY "Total Paid ",
                    arrs_expense[arr_curr()].total_paid_by_employee
                -- WHENEVER ERROR STOP
            END IF

            IF arrs_expense[arr_curr()].expense_type = "Miles" THEN 
                LET arrs_expense[arr_curr()].total_paid_by_employee = arrs_expense[arr_curr()].miles_total
                LET arrs_expense[arr_curr()].total_charged_to_fourjs =
                arrs_expense[arr_curr()].miles_total
            END IF 

    END INPUT

    LET expense_cnt = arrs_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF
END FUNCTION




FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arrs_expense[curr_pa].Exp_ID = employee_rec.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(arrs_expense[curr_pa].serial_no,
            arrs_expense[curr_pa].Exp_ID,
            arrs_expense[curr_pa].date,
            arrs_expense[curr_pa].location,
            arrs_expense[curr_pa].expense_type,
            arrs_expense[curr_pa].mileage,
            arrs_expense[curr_pa].amount,
            arrs_expense[curr_pa].charge_to_fourjs
)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = arrs_expense[curr_pa].date,
            location = arrs_expense[curr_pa].location,
            expense_type = arrs_expense[curr_pa].expense_type,
            milage = arrs_expense[curr_pa].mileage,
            amount = arrs_expense[curr_pa].amount,
            charged_to_fourjs = arrs_expense[curr_pa].charge_to_fourjs
        
        WHERE exp_details.serial_no = arrs_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arrs_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET arrs_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_ID = employee_rec.exp_id
            LET busn[curr_pa1].business_date = TODAY
            

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD business_date
            IF opflag = "U" THEN
                NEXT FIELD business_amount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF
END FUNCTION


FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arrs_expense[g].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            date = busn[curr_pa].business_date,
            amount = busn[curr_pa].business_amount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION





------Report
FUNCTION print_fun()
    DEFINE rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,


        Transport VARCHAR(50),
        Miles VARCHAR(50),
        Meals VARCHAR(50),
        AutoRent VARCHAR(50),
        Lodging VARCHAR(50),
        Fuel VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Entertainment VARCHAR(50),
        Airfare VARCHAR(50),
        Others VARCHAR(50)

    END RECORD

    IF fgl_report_loadCurrentSettings("reporter.4rp") THEN
        CALL fgl_report_selectDevice("PDF")

        CALL fgl_report_selectPreview(TRUE)
        LET handl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT Report_Emp TO XML HANDLER handl

    DECLARE curs1 CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * CAL_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            --exp_details.amount total_paid_by_employee,
            --exp_details.mileage * CAL_TAX miles_total,
            --exp_details.amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee,
            business_details.business_date,
            business_details.business_amount,
            business_details.company_name,
            business_details.business_trans
            FROM  exp_header, emp_mast, exp_details,business_details
            WHERE exp_details.exp_id = exp_header.exp_id
            AND exp_header.exp_id = employee_rec.exp_id AND emp_mast.emp_no = employee_rec.emp_no
            GROUP BY exp_details.serial_no
        LET total=0
        LET totalc=0
    FOREACH curs1 INTO rec.*
     IF rec.expense_type = "Transport" THEN
            LET rec.Transport = rec.amount
            

            END IF
            IF rec.expense_type = "Miles" THEN
            LET rec.Miles = rec.amount
           LET rec.total_paid_by_employee = rec.miles_total 
            END IF
            IF rec.expense_type = "Meals" THEN
            LET rec.Meals = rec.amount

            END IF
            IF rec.expense_type = "AutoRent" THEN
            LET rec.AutoRent = rec.amount

            END IF
            IF rec.expense_type = "Lodging" THEN
            LET rec.Lodging = rec.amount

            END IF
            IF rec.expense_type = "Fuel" THEN
            LET rec.Fuel = rec.amount

            END IF
            IF rec.expense_type = "Tolls" THEN
            LET rec.Tolls = rec.amount
            
            END IF
            IF rec.expense_type = "Phone" THEN
            LET rec.Phone = rec.amount
            
            END IF
            IF rec.expense_type = "Entertainment" THEN
            LET rec.Entertainment = rec.amount
            END IF

            IF rec.expense_type = "Airfare" THEN 
            LET rec.Airfare = rec.amount
            --LET rec.Airfare = rec.amount
            LET rec.total_paid_by_employee = 0
            END IF 
            
            IF rec.expense_type = "Others" THEN
            LET rec.Others = rec.amount
            END IF 
            LET total = total + rec.total_paid_by_employee
            LET totalc = totalc + rec.total_charged_to_fourjs
        OUTPUT TO REPORT Report_Emp(rec.*)
       
    END FOREACH

    FINISH REPORT Report_Emp
END FUNCTION

REPORT Report_Emp(myrec)
DEFINE g_total int
    DEFINE myrec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,

        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,

        Transport VARCHAR(50),
        Miles VARCHAR(50),
        Meals VARCHAR(50),
        AutoRent VARCHAR(50),
        Lodging VARCHAR(50),
        Fuel VARCHAR(50),
        Tolls VARCHAR(50),
        Phone VARCHAR(50),
        Entertainment VARCHAR(50),
        Airfare VARCHAR(50),
        Others VARCHAR(50)

    END RECORD

    DEFINE grand_total DECIMAL(9,2)
   
    ORDER EXTERNAL BY myrec.exp_id

    FORMAT
        FIRST PAGE HEADER
        PAGE HEADER
            PRINTX TODAYDATE
        ON EVERY ROW
         display" grand_total : ",g_total
         
         LET  grand_total =myrec.charge_to_fourjs+ myrec.total_paid_by_employee+myrec.total_charged_to_fourjs
            DISPLAY "myrec.*", myrec.exp_id
           
            PRINTX  myrec.exp_id,
                myrec.emp_no,
                myrec.notes,
                myrec.charge_type,
                myrec.emp_name,
                myrec.position,
                myrec.manager,
                myrec.department,
                myrec.date,
                myrec.location,
                myrec.expense_type,
                myrec.mileage,
                myrec.amount,
                myrec.charge_to_fourjs,
                myrec.miles_total,
                myrec.total_paid_by_employee,
                myrec.total_charged_to_fourjs,
                myrec.business_date,
                myrec.business_amount,
                myrec.company_name,
                myrec.business_trans,
                myrec.Transport,
                myrec.Miles,
                myrec.Meals,
                myrec.AutoRent,
                myrec.Lodging,
                myrec.Fuel,
                myrec.Tolls,
                myrec.Phone,
                myrec.Entertainment,
                myrec.Airfare,
                myrec.Others
                
                

            LET TODAYDATE = DATE(CURRENT)
            PRINTX TODAYDATE
            PRINTX total
            PRINTX totalc
            
            PRINTX rpt,
                "-------------------------END REPORT------------------------"

        

END REPORT

--end report

