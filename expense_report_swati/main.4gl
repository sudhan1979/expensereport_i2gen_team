IMPORT FGL zoom
DATABASE sprint

DEFINE handl om.SaxDocumenthandler
DEFINE today_date DATE

GLOBALS
    DEFINE
        g_rec_emp RECORD
            exp_id LIKE exp_header.exp_id,
            emp_no LIKE exp_header.emp_no,
            notes LIKE exp_header.notes,
            charge_type LIKE exp_header.charge_type,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,
            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,
        g_arr_expense DYNAMIC ARRAY OF RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            date LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            -- miles_total DECIMAL(9, 2),
            total_charged_to_fourjs DECIMAL(9, 2),
            total_paid_by_employee DECIMAL(9, 2)
        END RECORD
    TYPE g_rec_bus RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        date LIKE business_details.date,
        amount LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans

    END RECORD
    DEFINE g_arr_bus DYNAMIC ARRAY OF g_rec_bus
    DEFINE total_emp DECIMAL(16, 2)
    DEFINE total_fourjs DECIMAL(16, 2)

END GLOBALS

CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575

MAIN
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT

    CLOSE WINDOW SCREEN
    OPEN WINDOW Emp WITH FORM "main"
    MENU
        ON ACTION ADD
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL g_arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION FIND
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
        ON ACTION PRINT
            CALL print_fun()
            CLEAR FORM
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW Emp
END MAIN

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE g_rec_emp.* TO NULL
    SELECT MAX(exp_id) + 1 INTO g_rec_emp.exp_id FROM exp_header
    IF g_rec_emp.exp_id IS NULL OR g_rec_emp.exp_id == 0 THEN
        LET g_rec_emp.exp_id = 100
    END IF

    LET int_flag = FALSE
    INPUT BY NAME g_rec_emp.exp_id,
        g_rec_emp.emp_no,
        g_rec_emp.notes,
        g_rec_emp.charge_type,
        g_rec_emp.emp_name,
        g_rec_emp.position,
        g_rec_emp.manager,
        g_rec_emp.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        ON CHANGE emp_no
            SELECT *
                INTO g_rec_emp.*
                FROM exp_header
                WHERE emp_no = g_rec_emp.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            IF g_rec_emp.notes IS NULL THEN
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF
            IF NOT g_rec_emp.notes MATCHES "[a-zA-Z]*" THEN
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF

        ON ACTION zoom
            CALL display_emplist(
                ) RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET g_rec_emp.emp_no = id
                LET g_rec_emp.emp_name = NAME
                LET g_rec_emp.position = position
                LET g_rec_emp.manager = manager
                LET g_rec_emp.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME g_rec_emp.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id, emp_no, notes, charge_type)
        VALUES(g_rec_emp.exp_id,
            g_rec_emp.emp_no,
            g_rec_emp.notes,
            g_rec_emp.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added-----Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
            || "exp_header.exp_id,"
            || "exp_header.emp_no, "
            || "exp_header.notes, "
            || "exp_header.charge_type, "
            || "emp_mast.emp_name, "
            || "emp_mast.position, "
            || "emp_mast.manager, "
            || "emp_mast.department "
            || "FROM exp_header, emp_mast, exp_details "
            || "WHERE exp_header.exp_id = exp_details.exp_id and emp_mast.emp_no = exp_header.emp_no "
            || "AND "
            || where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO g_rec_emp.*
    ELSE
        FETCH PREVIOUS emp_curs INTO g_rec_emp.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME g_rec_emp.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            -- miles_total DECIMAL(9, 2),
            total_charged_to_fourjs DECIMAL(9, 2),
            total_paid_by_employee DECIMAL(9, 2)
        END RECORD

    IF g_rec_emp.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            --exp_details.mileage * C_TAX miles_total,
            -- exp_details.amount +
            exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee
            FROM exp_details
            WHERE exp_details.Exp_ID = g_rec_emp.exp_id

    LET expense_cnt = 0
    CALL g_arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET g_arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            date LIKE business_details.date,
            amount LIKE business_details.amount,
            company_name LIKE business_details.company_name,
            bussiness_trans LIKE business_details.business_trans

        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM business_details
            WHERE business_details.Exp_ID = g_rec_emp.exp_id

    LET bussiness_cnt = 0
    CALL g_arr_bus.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET g_arr_bus[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY g_arr_bus TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY g_arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expense_inpupd()

    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = g_arr_expense.getLength()

    INPUT ARRAY g_arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET g_arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO g_arr_expense[curr_pa].serial_no
                FROM exp_details
            IF g_arr_expense[curr_pa].serial_no IS NULL
                OR g_arr_expense[curr_pa].serial_no == 0 THEN
                LET g_arr_expense[curr_pa].serial_no = 1
            END IF
            LET g_arr_expense[curr_pa].Exp_ID = g_rec_emp.exp_id
            LET g_arr_expense[curr_pa].amount = 0
            LET g_arr_expense[curr_pa].date = TODAY

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD expense_type
            IF (g_arr_expense[arr_curr()].expense_type IS NULL) THEN
                ERROR "this feild cant be null"
                NEXT FIELD expense_type
            END IF
            IF (g_arr_expense[arr_curr()].expense_type MATCHES "MILES") THEN
                NEXT FIELD mileage
            END IF
            NEXT FIELD amount

        AFTER FIELD amount
            IF g_arr_expense[arr_curr()].expense_type MATCHES "MILES" THEN
                LET g_arr_expense[arr_curr()].amount =
                    0.575 * g_arr_expense[arr_curr()].mileage
            END IF

-- ON CHANGE mileage
-- WHENEVER ERROR CONTINUE
-- -- LET g_arr_expense[arr_curr()].miles_total =
-- -- C_TAX * g_arr_expense[arr_curr()].mileage
--
-- WHENEVER ERROR STOP
-- IF (SQLCA.SQLCODE = 0) THEN
-- DISPLAY BY NAME g_arr_expense[arr_curr()].*
-- --CALL DIALOG.setFieldActive("amount",FALSE)
-- END IF

        AFTER FIELD charge_to_fourjs
            IF g_arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            IF g_arr_expense[arr_curr()].expense_type MATCHES "AIR-FARE" THEN
                LET g_arr_expense[arr_curr()].total_charged_to_fourjs =
                    g_arr_expense[arr_curr()].amount
                        + g_arr_expense[arr_curr()].charge_to_fourjs
            ELSE
                LET g_arr_expense[arr_curr()].total_charged_to_fourjs =
                    g_arr_expense[arr_curr()].charge_to_fourjs
                DISPLAY "Total Charged ",
                    g_arr_expense[arr_curr()].total_charged_to_fourjs
            END IF

            IF g_arr_expense[arr_curr()].expense_type MATCHES "AIR-FARE" THEN
                LET g_arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                LET g_arr_expense[arr_curr()].total_paid_by_employee =
                    g_arr_expense[arr_curr()].amount

                DISPLAY "Total Paid ",
                    g_arr_expense[arr_curr()].total_paid_by_employee
            END IF

    END INPUT

    LET expense_cnt = g_arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET g_arr_expense[curr_pa].Exp_ID = g_rec_emp.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(g_arr_expense[curr_pa].serial_no,
            g_arr_expense[curr_pa].Exp_ID,
            g_arr_expense[curr_pa].date,
            g_arr_expense[curr_pa].location,
            g_arr_expense[curr_pa].expense_type,
            g_arr_expense[curr_pa].mileage,
            g_arr_expense[curr_pa].amount,
            g_arr_expense[curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = g_arr_expense[curr_pa].date,
            location = g_arr_expense[curr_pa].location,
            expense_type = g_arr_expense[curr_pa].expense_type,
            mileage = g_arr_expense[curr_pa].mileage,
            amount = g_arr_expense[curr_pa].amount,
            charge_to_fourjs = g_arr_expense[curr_pa].charge_to_fourjs
        WHERE exp_details.serial_no = g_arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = g_arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = g_arr_bus.getLength()

    INPUT ARRAY g_arr_bus
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET g_arr_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO g_arr_bus[curr_pa1].serial_no
                FROM business_details
            IF g_arr_bus[curr_pa1].serial_no IS NULL
                OR g_arr_bus[curr_pa1].serial_no == 0 THEN
                LET g_arr_bus[curr_pa1].serial_no = 1
            END IF
            LET g_arr_bus[curr_pa1].exp_ID = g_rec_emp.exp_id
            --LET g_arr_expense[curr_pa1].amount = 0
            LET g_arr_bus[curr_pa1].date = TODAY
            -- LET g_arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD date
            IF opflag = "U" THEN
                NEXT FIELD amount
            END IF
        AFTER FIELD company_name
            IF g_arr_bus[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = g_arr_bus.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET g_arr_bus[g].exp_id = g_arr_expense[g].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(g_arr_bus[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = g_arr_bus[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = g_arr_bus[curr_pa].serial_no,
            date = g_arr_bus[curr_pa].date,
            amount = g_arr_bus[curr_pa].amount,
            company_name = g_arr_bus[curr_pa].company_name,
            business_trans = g_arr_bus[curr_pa].business_trans,
            exp_id = g_arr_bus[curr_pa].exp_id
        WHERE business_details.serial_no = g_arr_bus[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME g_arr_bus[d].*
END FUNCTION

FUNCTION print_fun()
    DEFINE rec RECORD

        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,

        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        serial_no LIKE exp_details.serial_no,

        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,

        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        miles_total DECIMAL,
        total_charge_to_fourjs DECIMAL,
        total_paid_by_employee DECIMAL,

        AIR_FARE VARCHAR(20),
        ENTERTAINEMENT VARCHAR(20),
        MEALS VARCHAR(20),
        LODGING VARCHAR(20),
        AUTO_RENT VARCHAR(20),
        FUEL VARCHAR(20),
        TRANSPORT VARCHAR(20),
        TOLLS_PK VARCHAR(20),
        MILES VARCHAR(20),
        PHONE VARCHAR(20),
        OTHERS VARCHAR(20),
        total_emp DECIMAL,
        total_fourjs DECIMAL

    END RECORD

    IF fgl_report_loadCurrentSettings(" finalreport.4rp") THEN
        CALL fgl_report_selectDevice("PDF")

        CALL fgl_report_selectPreview(TRUE)
        LET handl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT Report_Emp TO XML HANDLER handl

    DECLARE curs123 CURSOR FOR
        SELECT business_details.company_name,
            business_details.business_trans,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_details.serial_no,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type,
            exp_details.mileage * C_TAX miles_total,
            exp_details.charge_to_fourjs total_charge_to_fourjs,
            exp_details.amount total_paid_by_employee
            FROM business_details, emp_mast, exp_details, exp_header
            WHERE exp_details.exp_id = g_rec_emp.exp_id
                AND exp_header.exp_id = g_rec_emp.exp_id
                AND emp_mast.emp_no = g_rec_emp.emp_no
            GROUP BY exp_details.serial_no

    LET total_emp = 0
    LET total_fourjs = 0

    FOREACH curs123 INTO rec.*

        IF rec.expense_type = "AIR_FARE" THEN
            LET rec.AIR_FARE = rec.amount

        END IF

        IF rec.expense_type = "ENTERTANMENT" THEN
            LET rec.ENTERTAINEMENT = rec.amount

        END IF

        IF rec.expense_type = "MEALS" THEN
            LET rec.MEALS = rec.amount

        END IF

        IF rec.expense_type = "LODGING" THEN
            LET rec.LODGING = rec.amount

        END IF

        IF rec.expense_type = "AUTO_RENT" THEN
            LET rec.AUTO_RENT = rec.amount

        END IF

        IF rec.expense_type = "FUEL" THEN
            LET rec.FUEL = rec.amount

        END IF

        IF rec.expense_type = "TRANSPORT" THEN
            LET rec.TRANSPORT = rec.amount

        END IF

        IF rec.expense_type = "TOLLS_PK" THEN
            LET rec.TOLLS_PK = rec.amount

        END IF

        IF rec.expense_type = "MILES" THEN
            LET rec.MILES = rec.amount

        END IF

        IF rec.expense_type = "PHONE" THEN
            LET rec.PHONE = rec.amount

        END IF

        IF rec.expense_type = "OTHERS" THEN
            LET rec.OTHERS = rec.amount

        END IF
        LET total_emp = total_emp + rec.total_paid_by_employee
        LET total_fourjs = total_fourjs + rec.total_charge_to_fourjs

        OUTPUT TO REPORT Report_Emp(rec.*)
    END FOREACH

    FINISH REPORT Report_Emp
END FUNCTION

REPORT Report_Emp(my)
    DEFINE my RECORD

        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,

        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        serial_no LIKE exp_details.serial_no,
        date LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,

        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        miles_total DECIMAL,
        total_charge_to_fourjs DECIMAL,
        total_paid_by_employee DECIMAL,

        AIR_FARE VARCHAR(20),
        ENTERTAINEMENT VARCHAR(20),
        MEALS VARCHAR(20),
        LODGING VARCHAR(20),
        AUTO_RENT VARCHAR(20),
        FUEL VARCHAR(20),
        TRANSPORT VARCHAR(20),
        TOLLS_PK VARCHAR(20),
        MILES VARCHAR(20),
        PHONE VARCHAR(20),
        OTHERS VARCHAR(20),
        total_emp DECIMAL,
        total_fourjs DECIMAL

    END RECORD

    ORDER EXTERNAL BY my.exp_id

    FORMAT
        FIRST PAGE HEADER
        PAGE HEADER

            PRINTX today_date
        ON EVERY ROW
            LET today_date = DATE(TODAY)
            LET my.date = today_date
            DISPLAY "Today Date,", today_date
            PRINTX today_date
            LET today_date = DATE(TODAY)
            LET my.date = today_date

            PRINTX my.company_name,
                my.business_trans,
                my.emp_name,
                my.position,
                my.manager,
                my.department,
                my.serial_no,
                my.date,
                my.location,
                my.expense_type,
                my.mileage,
                my.amount,
                my.charge_to_fourjs,
                my.exp_id,
                my.emp_no,
                my.notes,
                my.charge_type,
                my.total_charge_to_fourjs,
                my.total_paid_by_employee,
                my.miles_total,
                my.AIR_FARE,
                my.ENTERTAINEMENT,
                my.MEALS,
                my.LODGING,
                my.AUTO_RENT,
                my.FUEL,
                my.TRANSPORT,
                my.TOLLS_PK,
                my.MILES,
                my.PHONE,
                my.OTHERS,
                my.total_emp,
                my.total_fourjs

            PRINTX "-------------------------END REPORT------------------------"

        AFTER GROUP OF my.exp_id
            PRINTX my.exp_id, "FORM RECORDS"
            PRINTX total_emp
            PRINTX total_fourjs
            SKIP 2 LINES

END REPORT
