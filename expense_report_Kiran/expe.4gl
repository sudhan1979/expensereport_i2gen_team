SCHEMA expanse

FUNCTION display_emplist()
    DEFINE
        emp_arr DYNAMIC ARRAY OF RECORD
            emp_no LIKE emp_mast.emp_no,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,
            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,
        emp_rec RECORD
            emp_no LIKE emp_mast.emp_no,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,

            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,
        ret_num LIKE emp_mast.emp_no,
        ret_name LIKE emp_mast.emp_name,
        ret_position LIKE emp_mast.position,
        ret_manager LIKE emp_mast.manager,
        ret_department LIKE emp_mast.department,
        curr_pa, idx SMALLINT

    OPEN WINDOW expe WITH FORM "expe"

    DECLARE emp_curs CURSOR FOR
        SELECT emp_no, emp_name, position, manager, department
            FROM emp_mast
            ORDER BY emp_no

    LET idx = 0
    CALL emp_arr.clear()
    FOREACH emp_curs INTO emp_rec.*
        LET idx = idx + 1
        LET emp_arr[idx].* = emp_rec.*
    END FOREACH

    LET ret_num = 0
    LET ret_name = NULL

    IF idx > 0 THEN
        LET int_flag = FALSE
        DISPLAY ARRAY emp_arr TO sa_cust.* ATTRIBUTES(COUNT = idx)
        IF (NOT int_flag) THEN
            LET curr_pa = arr_curr()
            LET ret_num = emp_arr[curr_pa].emp_no
            LET ret_name = emp_arr[curr_pa].emp_name
            LET ret_position = emp_arr[curr_pa].position
            LET ret_manager = emp_arr[curr_pa].manager
            LET ret_department = emp_arr[curr_pa].department
        END IF
    END IF

    CLOSE WINDOW expe

    RETURN ret_num, ret_name, ret_position, ret_manager, ret_department

END FUNCTION
