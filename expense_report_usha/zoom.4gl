database expense

FUNCTION display_emplist()

    DEFINE
        emp_mast_arr DYNAMIC ARRAY OF RECORD
            emp_no LIKE emp_mast.emp_no,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,
            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,
        cust_rec RECORD
            emp_no LIKE emp_mast.emp_no,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,
            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,

        ret_num LIKE emp_mast.emp_no,
        ret_name LIKE emp_mast.emp_name,
        ret_pos LIKE emp_mast.position,
        ret_man LIKE emp_mast.manager,
        ret_dept LIKE emp_mast.department,
        curr_pa, idx SMALLINT

    OPEN WINDOW wcust WITH FORM "zoom"

    DECLARE custlist_curs CURSOR FOR SELECT *
            FROM emp_mast
            ORDER BY emp_no

    LET idx = 0
    CALL emp_mast_arr.clear()
    FOREACH custlist_curs INTO cust_rec.*
        LET idx = idx + 1
        LET emp_mast_arr[idx].* = cust_rec.*
    END FOREACH

    LET ret_num = 0
    LET ret_name = NULL

    IF idx > 0 THEN
        LET int_flag = FALSE
        DISPLAY ARRAY emp_mast_arr TO sa_cust.* ATTRIBUTES(COUNT = idx)
        IF (NOT int_flag) THEN
            LET curr_pa = arr_curr()
            LET ret_num = emp_mast_arr[curr_pa].emp_no
            LET ret_name = emp_mast_arr[curr_pa].emp_name
            LET ret_pos = emp_mast_arr[curr_pa].position
            LET ret_man = emp_mast_arr[curr_pa].manager
            LET ret_dept = emp_mast_arr[curr_pa].department
        END IF
    END IF

    CLOSE WINDOW wcust

    RETURN ret_num, ret_name, ret_pos, ret_man, ret_dept

END FUNCTION
