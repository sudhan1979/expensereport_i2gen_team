"use strict";

modulum('mybuttonwidget', ['EditWidget', 'WidgetFactory'],
function(context, cls) {

cls.mybuttonwidget= context.oo.Class(cls.EditWidget, function($super) {
return {
__name: "mybuttonwidget",

// your custom code /
};
});
cls.WidgetFactory.register('Edit', cls.mybuttonwidget);
});