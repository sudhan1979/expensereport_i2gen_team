"use strict";

modulum('MyImageWidget', ['ImageWidget', 'WidgetFactory'],
function(context, cls) {

cls.mybuttonwidget= context.oo.Class(cls.ImageWidget, function($super) {
return {
__name: "MyImagewidget",

// your custom code /
};
});
cls.WidgetFactory.register('ImageWidget', cls.MyImagewidget);
});