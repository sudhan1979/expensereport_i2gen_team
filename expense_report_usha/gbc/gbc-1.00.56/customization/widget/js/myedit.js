"use strict";

modulum('myedit', ['DateEditWidget', 'WidgetFactory'],
function(context, cls) {

cls.myedit= context.oo.Class(cls.DateEditWidget, function($super) {
return {
__name: "myedit",

// your custom code /
};
});
cls.WidgetFactory.register('DateEdit', cls.myedit);
});