"use strict";

modulum('mybuttonwidget', ['DateEditWidget', 'WidgetFactory'],
function(context, cls) {

cls.mybuttonwidget= context.oo.Class(cls.DateEditWidget, function($super) {
return {
__name: "mybuttonwidget",

// your custom code /
};
});
cls.WidgetFactory.register('DateEditWidget', cls.mybuttonwidget);
});