"use strict";

modulum('MyTableProjectWidget', ['TableWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.MyTableProjectWidget = context.oo.Class(cls.TableWidget, function($super) {
      return {
        __name: "MyTableProjectWidget",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('Table', 'table_style',cls.MyTableProjectWidget);
    // cls.WidgetFactory.registerBuilder('EditWidget', cls.MyEditWidget);
  });
