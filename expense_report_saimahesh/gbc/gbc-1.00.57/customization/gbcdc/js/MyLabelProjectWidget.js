"use strict";

modulum('MyLabelProjectWidget', ['LabelWidget', 'WidgetFactory'],
function(context, cls) {

cls.MyLabelProjectWidget = context.oo.Class(cls.LabelWidget, function($super) {
return {
__name: "MyLabelProjectWidget",

/* your custom code */
};
});
cls.WidgetFactory.register('Label', 'edit_name',cls.MyLabelProjectWidget);
});