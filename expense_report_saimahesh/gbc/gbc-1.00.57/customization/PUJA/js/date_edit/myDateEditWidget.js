"use strict";

modulum('myDateEditWidget', ['DateEditWidget', 'WidgetFactory'],
  function(context, cls) {

    cls.myDateEditWidget = context.oo.Class(cls.DateEditWidget, function($super) {
      return {
        __name: "myDateEditWidget",
       // __templateName: "newTemplate",

        /* your custom code */
      };
    });
    cls.WidgetFactory.register('DateEdit','edit_date',cls.myDateEditWidget);
  });
