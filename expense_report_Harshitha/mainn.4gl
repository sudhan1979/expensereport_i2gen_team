IMPORT FGL exp_zoom
DATABASE expense

GLOBALS 

 DEFINE arr_index INTEGER
    DEFINE TODAYDATE DATE
    DEFINE rpt VARCHAR(200)
    DEFINE prt VARCHAR(200)
    DEFINE hndl om.SaxDocumentHandler
    DEFINE cmb ui.ComboBox
    DEFINE list DYNAMIC ARRAY OF INTEGER
    DEFINE cnt INTEGER
    DEFINE d ui.Dialog
    DEFINE answer STRING 

    
    DEFINE
    emp_rec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        --emp_no1 LIKE emp_mast.emp_no,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department
    END RECORD,
    arr_expense DYNAMIC ARRAY OF RECORD
        serial_no LIKE exp_details.serial_no,
        Exp_ID LIKE exp_details.exp_ID,
        exp_date LIKE exp_details.exp_date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        exp_amount LIKE exp_details.exp_amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2)
    END RECORD
    TYPE busi RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans
        
    END RECORD
DEFINE busn DYNAMIC ARRAY OF busi
DEFINE total DECIMAL(10,2)
DEFINE totalc DECIMAL(10,2)
END GLOBALS 
CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575


MAIN 
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
    --CALL ui.Interface.loadStyles("styles")
    --CALL ui.Interface.loadActionDefaults("actions")
    --CALL ui.Form.setDefaultInitializer("initializeForm")
    CLOSE WINDOW SCREEN
    OPEN WINDOW expense_report WITH FORM "asd"
    MENU
        ON ACTION NEW
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok

            ON ACTION NEXT
            CALL emp_fetch_rel(1)

            ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM

        
        COMMAND "print"
            CALL print_expense()
            
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW expense_report 
END MAIN 

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE emp_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO emp_rec.exp_id FROM exp_header
    IF emp_rec.exp_id IS NULL OR emp_rec.exp_id == 0 THEN
        LET emp_rec.exp_id = 100
    END IF

    
    LET int_flag = FALSE
    INPUT BY NAME emp_rec.exp_id,
        emp_rec.emp_no,
        emp_rec.notes,
        emp_rec.charge_type,
        emp_rec.emp_name,
        emp_rec.position,
        emp_rec.manager,
        emp_rec.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        
        ON CHANGE emp_no
            SELECT * INTO emp_rec.* FROM exp_header WHERE emp_no = emp_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            IF emp_rec.notes IS NULL THEN 
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF 
            IF NOT emp_rec.notes MATCHES "[a-zA-Z]*" THEN 
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF 

        ON ACTION zoom
            CALL display_emplist(
                ) RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET emp_rec.emp_no = id
                LET emp_rec.emp_name = NAME
                LET emp_rec.position = position
                LET emp_rec.manager = manager
                LET emp_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME emp_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id,
        emp_no,
        notes,
        charge_type)
        VALUES(emp_rec.exp_id,
            emp_rec.emp_no,
            emp_rec.notes,
            emp_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Details  Added"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
            || "exp_header.exp_id, "
            || "exp_header.emp_no, "
            || "exp_header.notes,  "
            || "exp_header.charge_type, "
            || "emp_mast.emp_name, "
            || "emp_mast.position, "
            || "emp_mast.manager, "
            || "emp_mast.department "
            || "FROM exp_header, exp_details, emp_mast "
            || "WHERE exp_header.exp_id = exp_details.exp_id "
            || "AND "
            || where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE
END FUNCTION


FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO emp_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO emp_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME emp_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            exp_DATE LIKE exp_details.exp_date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            exp_amount LIKE exp_details.exp_amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9,2),
            total_paid_by_employee DECIMAL(9,2)
        END RECORD

    IF emp_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.exp_date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.exp_amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.exp_amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.exp_amount total_paid_by_employee
            FROM exp_details
            WHERE exp_details.Exp_ID = emp_rec.exp_id

    LET expense_cnt = 0
    CALL arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            business_date LIKE business_details.business_date,
            business_amount LIKE business_details.business_amount,
            company_name LIKE business_details.company_name,
            business_trans LIKE business_details.business_trans
            
        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
        business_details.exp_id,
            business_details.business_date,
            business_details.business_amount,
            business_details.company_name,
            business_details.business_trans
            
            FROM business_details
            WHERE business_details.exp_id = emp_rec.exp_id

    LET bussiness_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET busn[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expense_inpupd()
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arr_expense.getLength()

    INPUT ARRAY arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*

        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
      --  BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arr_expense[curr_pa].serial_no
                FROM exp_details
            IF arr_expense[curr_pa].serial_no IS NULL
                OR arr_expense[curr_pa].serial_no == 0 THEN
                LET arr_expense[curr_pa].serial_no = 1
            END IF
            LET arr_expense[curr_pa].Exp_ID = emp_rec.exp_id
            LET arr_expense[curr_pa].exp_amount = 0
            LET arr_expense[curr_pa].exp_date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD exp_date
            END IF
        AFTER FIELD expense_type
            IF (arr_expense[arr_curr()].expense_type = 105) THEN
                MESSAGE "Choose one expense_type"
                CALL DIALOG.setFieldActive("exp_amount", 0)
                NEXT FIELD mileage
            ELSE
                CALL DIALOG.setFieldActive("exp_amount", 1)
                NEXT FIELD exp_amount
                
            END IF

            -- NEXT FIELD charged_to_fourjs
            --AFTER FIELD milage
            --IF arr_expense[arr_curr()].milage IS NULL THEN
            --    MESSAGE "Please enter quantity"
            --    NEXT FIELD milage
            --END IF

--             ON CHANGE amount
--                  WHENEVER ERROR CONTINUE
--                  LET arr_expense[arr_curr()].total_charged_to_fourjs = arr_expense[arr_curr()].amount + arr_expense[arr_curr()].charged_to_fourjs
--
--                  LET arr_expense[arr_curr()].total_charged_to_fourjs = arr_expense[arr_curr()].total_paid_by_employee
--                  WHENEVER ERROR STOP
--                  IF (SQLCA.SQLCODE = 0)THEN
--                     CALL DIALOG.setFieldActive("milage",0)
--                    END IF
        ON CHANGE mileage
            WHENEVER ERROR CONTINUE
            LET arr_expense[arr_curr()].miles_total =
                C_TAX * arr_expense[arr_curr()].mileage

            WHENEVER ERROR STOP
            IF (SQLCA.SQLCODE = 0) THEN
                DISPLAY BY NAME arr_expense[arr_curr()].*
                --CALL DIALOG.setFieldActive("amount",FALSE)
            END IF
            -- NEXT FIELD amount
            --AFTER FIELD amount
            --  IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
            --    MESSAGE "Amount"
            --    NEXT FIELD amount
            --    END IF
        AFTER FIELD charge_to_fourjs
            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            -- WHENEVER ERROR CONTINUE
            LET arr_expense[arr_curr()].total_charged_to_fourjs =
                arr_expense[arr_curr()].exp_amount
                    + arr_expense[arr_curr()].charge_to_fourjs
            DISPLAY "Total Charged ",
                arr_expense[arr_curr()].total_charged_to_fourjs

            -- AFTER ROW
            LET arr_expense[arr_curr()].total_paid_by_employee = arr_expense[arr_curr()].miles_total
            -- AFTER INPUT --  DISPLAY "Total Charged ",recod[arr_curr()].total_charged_to_fourjs
            IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
                LET arr_expense[arr_curr()].total_paid_by_employee =
                    arr_expense[arr_curr()].exp_amount
                DISPLAY "Total Paid ",
                    arr_expense[arr_curr()].total_paid_by_employee
                -- WHENEVER ERROR STOP
            END IF

    END INPUT

    LET expense_cnt = arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION fill_data(cmb)

    DEFINE cmb ui.ComboBox
    CALL cmb.addItem(101, "Entertainment")
    CALL cmb.addItem(102, "Meals")
    CALL cmb.addItem(103, "Lodging")
    CALL cmb.addItem(104, "Auto Rent")
    CALL cmb.addItem(105, "Miles")
    CALL cmb.addItem(106, "Fuel")
    CALL cmb.addItem(107, "Transport")
    CALL cmb.addItem(108, "Tolls/PK")
    CALL cmb.addItem(109, "Phone")
    CALL cmb.addItem(110, "Others")
    CALL cmb.addItem(111, "Airfare")

END FUNCTION

FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arr_expense[curr_pa].Exp_ID = emp_rec.Exp_ID
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        exp_DATE,
        location,
        expense_type,
        Mileage,
        exp_amount,
        charge_to_fourjs)
        VALUES(arr_expense[curr_pa].serial_no,
            arr_expense[curr_pa].Exp_ID,
            arr_expense[curr_pa].exp_date,
            arr_expense[curr_pa].location,
            arr_expense[curr_pa].expense_type,
            arr_expense[curr_pa].mileage,
            arr_expense[curr_pa].exp_amount,
            arr_expense[curr_pa].charge_to_fourjs
)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET exp_date = arr_expense[curr_pa].exp_date,
            location = arr_expense[curr_pa].location,
            expense_type = arr_expense[curr_pa].expense_type,
            milage = arr_expense[curr_pa].mileage,
            exp_amount = arr_expense[curr_pa].exp_amount,
            charged_to_fourjs = arr_expense[curr_pa].charge_to_fourjs
        
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION


FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

        WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET arr_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_ID = emp_rec.exp_id
            --LET arr_expense[curr_pa1].amount = 0
            LET busn[curr_pa1].business_date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD business_date
            IF opflag = "U" THEN
                NEXT FIELD business_amount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                --CALL DIALOG.setFieldActive("amount",0)
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION


FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arr_expense[g].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION


FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            business_date = busn[curr_pa].business_date,
            business_amount = busn[curr_pa].business_amount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION


FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION


FUNCTION print_expense()

    DEFINE itemsrec RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        exp_date LIKE exp_details.exp_date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        exp_amount LIKE exp_details.exp_amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),
       
        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans

    END RECORD
    
    IF fgl_report_loadCurrentSettings("reporte.4rp") THEN
        CALL fgl_report_selectDevice("PDF")
        CALL fgl_report_selectPreview(TRUE)
        LET hndl = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF

    START REPORT custreport TO XML HANDLER hndl

    DECLARE itemc CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type,
            
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,

            exp_details.exp_date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.exp_amount,
            exp_details.charge_to_fourjs,
            exp_details.mileage * C_TAX miles_total,
            exp_details.exp_amount + exp_details.charge_to_fourjs total_charged_to_fourjs,
            exp_details.exp_amount total_paid_by_employee,

            business_details.business_date,
            business_details.business_amount,
            business_details.company_name,
            business_details.business_trans             

            FROM exp_header,emp_mast,exp_details,business_details
            
            WHERE exp_details.exp_id = emp_rec.exp_id
            AND exp_header.exp_id = emp_rec.exp_id
            GROUP BY exp_details.serial_no

            LET total=0
            LET totalc=0
    FOREACH itemc INTO itemsrec.*

    
        OUTPUT TO REPORT custreport(itemsrec.*)
    END FOREACH
    FINISH REPORT custreport
END FUNCTION

REPORT custreport(printitemr)
    DEFINE printitemr RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,
        
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        exp_date LIKE exp_details.exp_date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        exp_amount LIKE exp_details.exp_amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,
        miles_total DECIMAL(9,2),
        total_charged_to_fourjs DECIMAL(9,2),
        total_paid_by_employee DECIMAL(9,2),

        business_date LIKE business_details.business_date,
        business_amount LIKE business_details.business_amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans

    END RECORD

    ORDER EXTERNAL BY printitemr.exp_id
    FORMAT
        FIRST PAGE HEADER
        PAGE HEADER
--LET createdate = DATE(CURRENT)
--PRINTX createdate
        ON EVERY ROW
            DISPLAY "printitemr", printitemr.exp_id
            PRINTX printitemr.emp_no,
                printitemr.notes,
                printitemr.charge_type,
                
                printitemr.emp_name,
                printitemr.position,
                printitemr.manager,
                printitemr.department,
                
                printitemr.exp_date,
                printitemr.location,
                printitemr.expense_type,
                printitemr.mileage,
                printitemr.exp_amount,
                printitemr.charge_to_fourjs,
                printitemr.miles_total,
                printitemr.total_paid_by_employee,
                printitemr.total_charged_to_fourjs,

                printitemr.business_date,
                printitemr.business_amount,
                printitemr.company_name,
                printitemr.business_trans

        --AFTER GROUP OF printitemr.employee_number
        --    PRINTX printitemr.employee_number, "FORM RECORD";
        --    SKIP 2 LINES
END REPORT
