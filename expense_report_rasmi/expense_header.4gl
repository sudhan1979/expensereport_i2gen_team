IMPORT FGL zoom1
DATABASE expense_excel

GLOBALS

    DEFINE hnd1 om.SaxDocumentHandler
    DEFINE createdate DATE
    DEFINE total_paid INTEGER
    DEFINE
        emp_rec RECORD
            exp_id LIKE exp_header.exp_id,
            emp_no LIKE exp_header.emp_no,
            notes LIKE exp_header.notes,
            charge_type LIKE exp_header.charge_type,
            emp_no1 LIKE emp_mast.emp_no1,
            emp_name LIKE emp_mast.emp_name,
            position LIKE emp_mast.position,
            manager LIKE emp_mast.manager,
            department LIKE emp_mast.department
        END RECORD,
        arr_expense DYNAMIC ARRAY OF RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.exp_id,
            date LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            --miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9, 2),
            total_paid_by_employee DECIMAL(9, 2)

             
        END RECORD
    TYPE busi RECORD
        serial_no LIKE business_details.serial_no,
        exp_id LIKE business_details.exp_id,
        date LIKE business_details.date,
        amount LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans

    END RECORD
    DEFINE busn DYNAMIC ARRAY OF busi
    DEFINE  total_cal DECIMAL(9,2)
    DEFINE total_cal1 DECIMAL(9,2) 

END GLOBALS
CONSTANT msg02 = "Enter Search Criteria"
CONSTANT msg03 = "Canceled By User"
CONSTANT msg04 = "No Rows Found in Table"
CONSTANT msg05 = "End Of List"
CONSTANT msg06 = "Begin Of List"

CONSTANT msg08 = "Row Added"
CONSTANT msg09 = "Row Updated"
CONSTANT msg10 = "Row Deleted"
CONSTANT msg11 = "Enter Employee"
CONSTANT msg12 = "This Employee Does Not Exists"
CONSTANT msg13 = "Bussiness Updated"
CONSTANT C_TAX = 0.575

MAIN
    DEFINE ha_employee, query_ok SMALLINT
    DEFER INTERRUPT
    --CALL ui.Interface.loadStyles("styles")
    --CALL ui.Interface.loadActionDefaults("actions")
    --CALL ui.Form.setDefaultInitializer("initializeForm")
    CLOSE WINDOW SCREEN
    OPEN WINDOW Emp WITH FORM "expense_header"
    MENU

        COMMAND "print"
            CALL print_expense()

        ON ACTION NEW
            CLEAR FORM
            LET query_ok = FALSE
            CALL close_emp()
            LET ha_employee = emp_new()
            IF ha_employee THEN
                CALL arr_expense.clear()
                CALL expense_inpupd()
                CALL input_buss()
            END IF
        ON ACTION Find
            CLEAR FORM
            LET query_ok = employee_query()
            LET ha_employee = query_ok
        ON ACTION NEXT
            CALL emp_fetch_rel(1)
        ON ACTION PREVIOUS
            CALL emp_fetch_rel(-1)
            CLEAR FORM
        ON ACTION EXIT
            EXIT MENU
    END MENU
    CLOSE WINDOW Emp
END MAIN

FUNCTION emp_new()
    DEFINE
        id INTEGER,
        name STRING,
        position STRING,
        manager STRING,
        department STRING
    MESSAGE msg11

    INITIALIZE emp_rec.* TO NULL
    SELECT MAX(exp_id) + 1 INTO emp_rec.exp_id FROM exp_header
    IF emp_rec.exp_id IS NULL OR emp_rec.exp_id == 0 THEN
        LET emp_rec.exp_id = 100
    END IF

    LET int_flag = FALSE
    INPUT BY NAME emp_rec.exp_id,
        emp_rec.emp_no,
        emp_rec.notes,
        emp_rec.charge_type,
        emp_rec.emp_name,
        emp_rec.position,
        emp_rec.manager,
        emp_rec.department
        WITHOUT DEFAULTS
        ATTRIBUTES(UNBUFFERED)

        ON CHANGE emp_no
            SELECT *
                INTO emp_rec.*
                FROM exp_header
                WHERE emp_no = emp_rec.emp_no
            IF (SQLCA.SQLCODE == NOTFOUND) THEN
                ERROR msg12
                NEXT FIELD emp_no
            END IF

        AFTER FIELD notes
            IF emp_rec.notes IS NULL THEN
                ERROR "notes should not null"
                NEXT FIELD notes
            END IF
            IF NOT emp_rec.notes MATCHES "[a-zA-Z]*" THEN
                ERROR "notes should not be null"
                NEXT FIELD notes
            END IF

        ON ACTION zoom1
            CALL display_emplist()
                RETURNING id, NAME, position, manager, department

            IF (id > 0) THEN
                LET emp_rec.emp_no = id
                LET emp_rec.emp_name = NAME
                LET emp_rec.position = position
                LET emp_rec.manager = manager
                LET emp_rec.department = department

            END IF

    END INPUT

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_insert()

END FUNCTION

FUNCTION employee_insert()

    --DISPLAY BY NAME emp_rec.*
    WHENEVER ERROR CONTINUE
    INSERT INTO exp_header(
        exp_id, emp_no, notes, charge_type)
        VALUES(emp_rec.exp_id,
            emp_rec.emp_no,
            emp_rec.notes,
            emp_rec.charge_type)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE <> 0) THEN
        CLEAR FORM
        ERROR SQLERRMESSAGE
        RETURN FALSE
    END IF

    MESSAGE "Employee Added-----Now You Add Expense Detail"
    RETURN TRUE

END FUNCTION

FUNCTION employee_query()
    DEFINE where_clause STRING
    MESSAGE msg02

    LET int_flag = FALSE
    CONSTRUCT BY NAME where_clause
        ON exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type

    IF (int_flag) THEN
        LET int_flag = FALSE
        CLEAR FORM
        MESSAGE msg03
        RETURN FALSE
    END IF

    RETURN employee_select(where_clause)

END FUNCTION

FUNCTION employee_select(where_clause)
    DEFINE
        where_clause STRING,
        sql_text STRING

    LET sql_text =
        "SELECT "
            || " exp_header.exp_id ,"
            || " exp_header.emp_no , "
            || " exp_header.notes ,  "
            || " exp_header.charge_type , "
            || " emp_mast.emp_no1 , "
            || " emp_mast.emp_name ,"
            || " emp_mast.position ,"
            || " emp_mast.manager ,"
            || " emp_mast.department "
            || " FROM exp_header,emp_mast, exp_details "
            || " WHERE exp_header.exp_id = exp_details.exp_id and emp_mast.emp_no1 = exp_header.emp_no "
            || " AND "
            || where_clause

    DECLARE emp_curs SCROLL CURSOR FROM sql_text
    OPEN emp_curs
    IF (NOT emp_fetch(1)) THEN
        CLEAR FORM
        MESSAGE msg04
        RETURN FALSE
    END IF

    RETURN TRUE

END FUNCTION

FUNCTION emp_fetch(p_fetch_flag)
    DEFINE p_fetch_flag SMALLINT

    IF p_fetch_flag = 1 THEN
        FETCH NEXT emp_curs INTO emp_rec.*
    ELSE
        FETCH PREVIOUS emp_curs INTO emp_rec.*
    END IF

    IF (SQLCA.SQLCODE == NOTFOUND) THEN
        RETURN FALSE
    END IF

    DISPLAY BY NAME emp_rec.*
    CALL expense_fetch()
    CALL Bussiness_fetch()
    RETURN TRUE

END FUNCTION

FUNCTION close_emp()
    WHENEVER ERROR CONTINUE
    CLOSE emp_curs
    WHENEVER ERROR STOP
END FUNCTION

FUNCTION emp_fetch_rel(p_fetch_flag)

    DEFINE p_fetch_flag SMALLINT

    MESSAGE " "
    IF (NOT emp_fetch(p_fetch_flag)) THEN
        IF (p_fetch_flag = 1) THEN
            MESSAGE msg05
        ELSE
            MESSAGE msg06
        END IF
    END IF

END FUNCTION

FUNCTION expense_fetch()
    DEFINE
        expense_cnt INTEGER,
        item_rec RECORD
            serial_no LIKE exp_details.serial_no,
            Exp_ID LIKE exp_details.Exp_ID,
            DATE LIKE exp_details.date,
            location LIKE exp_details.location,
            expense_type LIKE exp_details.expense_type,
            mileage LIKE exp_details.mileage,
            amount LIKE exp_details.amount,
            charge_to_fourjs LIKE exp_details.charge_to_fourjs,
            --miles_total DECIMAL(9,2),
            total_charged_to_fourjs DECIMAL(9, 2),
            total_paid_by_employee DECIMAL(9, 2)

            
        END RECORD

    IF emp_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE exp_curs CURSOR FOR
        SELECT exp_details.serial_no,
            exp_details.Exp_ID,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs
                total_charged_to_fourjs,
            exp_details.mileage * C_TAX total_charged_to_fourjs,
            exp_details.mileage * C_TAX total_paid_by_employee
            FROM exp_details
            WHERE exp_details.exp_id = emp_rec.exp_id

    LET expense_cnt = 0
    CALL arr_expense.clear()
    FOREACH exp_curs INTO item_rec.*
        LET expense_cnt = expense_cnt + 1
        LET arr_expense[expense_cnt].* = item_rec.*
    END FOREACH
    FREE exp_curs

    CALL expenseData_show()

END FUNCTION

FUNCTION Bussiness_fetch()
    DEFINE
        bussiness_cnt INTEGER,
        bus_rec RECORD
            serial_no LIKE business_details.serial_no,
            exp_id LIKE business_details.exp_id,
            date LIKE business_details.date,
            amount LIKE business_details.amount,
            company_name LIKE business_details.company_name,
            bussiness_trans LIKE business_details.business_trans

        END RECORD

    IF bus_rec.exp_id IS NULL THEN
        RETURN
    END IF

    DECLARE buss_curs CURSOR FOR
        SELECT business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans
            FROM business_details
            WHERE business_details.exp_id = emp_rec.exp_id

    LET bussiness_cnt = 0
    CALL busn.clear()
    FOREACH buss_curs INTO bus_rec.*
        LET bussiness_cnt = bussiness_cnt + 1
        LET busn[bussiness_cnt].* = bus_rec.*
    END FOREACH
    FREE buss_curs

    CALL bussData_show()

END FUNCTION

FUNCTION bussData_show()
    DISPLAY ARRAY busn TO sa_buss.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION expenseData_show()
    DISPLAY ARRAY arr_expense TO sa_exp.*
        BEFORE DISPLAY
            EXIT DISPLAY
    END DISPLAY
END FUNCTION

FUNCTION fill_data(cmb)

    DEFINE cmb ui.ComboBox
    CALL cmb.addItem(101, "Entertainment")
    CALL cmb.addItem(102, "Meals")
    CALL cmb.addItem(103, "Lodging")
    CALL cmb.addItem(104, "Auto Rent")
    CALL cmb.addItem(105, "Miles")
    CALL cmb.addItem(106, "Fuel")
    CALL cmb.addItem(107, "Transport")
    CALL cmb.addItem(108, "Tolls/PK")
    CALL cmb.addItem(109, "Phone")
    CALL cmb.addItem(110, "Others")
    CALL cmb.addItem(111, "Airfare")

END FUNCTION

FUNCTION expense_insert(curr_pa)
    DEFINE curr_pa SMALLINT

    LET curr_pa = arr_curr()

    WHENEVER ERROR CONTINUE
    LET arr_expense[curr_pa].Exp_ID = emp_rec.exp_id
    INSERT INTO exp_details(
        serial_no,
        Exp_ID,
        DATE,
        location,
        expense_type,
        Mileage,
        amount,
        charge_to_fourjs)
        VALUES(arr_expense[curr_pa].serial_no,
            arr_expense[curr_pa].Exp_ID,
            arr_expense[curr_pa].date,
            arr_expense[curr_pa].location,
            arr_expense[curr_pa].expense_type,
            arr_expense[curr_pa].mileage,
            arr_expense[curr_pa].amount,
            arr_expense[curr_pa].charge_to_fourjs)
    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg08
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE exp_details
        SET date = arr_expense[curr_pa].date,
            location = arr_expense[curr_pa].location,
            expense_type = arr_expense[curr_pa].expense_type,
            mileage = arr_expense[curr_pa].mileage,
            amount = arr_expense[curr_pa].amount,
            charge_to_fourjs = arr_expense[curr_pa].charge_to_fourjs
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg09
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION expense_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM exp_details
        WHERE exp_details.serial_no = arr_expense[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION input_buss()
    DEFINE
        opflag CHAR(1),
        buss_cnt, curr_pa1 SMALLINT

    LET opflag = "U"

    LET buss_cnt = busn.getLength()

    INPUT ARRAY busn
        WITHOUT DEFAULTS
        FROM sa_buss.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa1 = ARR_CURR()
            -- LET arr_expense[curr_pa1].milage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO busn[curr_pa1].serial_no
                FROM business_details
            IF busn[curr_pa1].serial_no IS NULL
                OR busn[curr_pa1].serial_no == 0 THEN
                LET busn[curr_pa1].serial_no = 1
            END IF
            LET busn[curr_pa1].exp_ID = emp_rec.exp_id
            --LET arr_expense[curr_pa1].amount = 0
            LET busn[curr_pa1].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            --      LET c_tax = 0.575

        AFTER INSERT
            CALL insert_buss(curr_pa1)

        BEFORE DELETE
            CALL buss_delete(curr_pa1)

        ON ROW CHANGE
            CALL buss_update(curr_pa1)

        BEFORE FIELD date
            IF opflag = "U" THEN
                NEXT FIELD amount
            END IF
        AFTER FIELD company_name
            IF busn[curr_pa1].company_name IS NULL THEN
                MESSAGE "Enter Company Name"
                --CALL DIALOG.setFieldActive("amount",0)
                NEXT FIELD company_name

            END IF

    END INPUT

    LET buss_cnt = busn.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION

FUNCTION insert_buss(g)
    DEFINE g SMALLINT
    LET g = arr_curr()
    LET busn[g].exp_id = arr_expense[g].Exp_ID
    WHENEVER ERROR CONTINUE
    INSERT INTO business_details VALUES(busn[g].*)

    WHENEVER ERROR STOP
    -- LET cnt = cnt + 1
    IF SQLCA.SQLCODE = 0 THEN
        MESSAGE "Row Added"
    ELSE
        ERROR SQLERRMESSAGE
    END IF
END FUNCTION

FUNCTION buss_delete(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    DELETE FROM business_details
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg10
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION buss_update(curr_pa)
    DEFINE curr_pa SMALLINT
    LET curr_pa = arr_curr()
    WHENEVER ERROR CONTINUE
    UPDATE business_details
        SET serial_no = busn[curr_pa].serial_no,
            date = busn[curr_pa].date,
            amount = busn[curr_pa].amount,
            company_name = busn[curr_pa].company_name,
            business_trans = busn[curr_pa].business_trans,
            exp_id = busn[curr_pa].exp_id
        WHERE business_details.serial_no = busn[curr_pa].serial_no

    WHENEVER ERROR STOP

    IF (SQLCA.SQLCODE == 0) THEN
        MESSAGE msg13
    ELSE
        ERROR SQLERRMESSAGE
    END IF

END FUNCTION

FUNCTION display_bus()
    DEFINE d INT
    LET d = 1
    DISPLAY BY NAME busn[d].*
END FUNCTION

FUNCTION print_expense()
    DEFINE cust_item_tranRec1 RECORD
        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        emp_no1 LIKE emp_mast.emp_no1,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        serial_no LIKE exp_details.serial_no,
        exp_id1 LIKE exp_details.exp_id,
        DATE LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,

        serial_no1 LIKE business_details.serial_no,
        exp_id2 LIKE business_details.exp_id,
        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
        miles_total DECIMAL(9, 2),
        total_charged_to_fourjs DECIMAL(9, 2),
        total_paid_by_employee DECIMAL(9, 2),

        Entertainment VARCHAR(30),
        Meals VARCHAR(30),
        Lodging VARCHAR(30),
        Auto_Rent VARCHAR(30),
        Miles VARCHAR(30),
        Fuel VARCHAR(30),
        Transport VARCHAR(30),
        Tolls VARCHAR(30),
        Phone VARCHAR(30),
        Others VARCHAR(30),
        Airfare VARCHAR(30)

    END RECORD
    #DEFINE exp_rec cust_item_tranRec1

    IF fgl_report_loadCurrentSettings("reporttttt.4rp") THEN
        CALL fgl_report_selectDevice("PDF")
        CALL fgl_report_selectPreview(TRUE)
        LET hnd1 = fgl_report_commitCurrentSettings()
    ELSE
        EXIT PROGRAM
    END IF
    START REPORT custpay_item_report TO XML HANDLER hnd1
    DECLARE custpay_item_tranCurr1 CURSOR FOR
        SELECT exp_header.exp_id,
            exp_header.emp_no,
            exp_header.notes,
            exp_header.charge_type,
            emp_mast.emp_no1,
            emp_mast.emp_name,
            emp_mast.position,
            emp_mast.manager,
            emp_mast.department,
            exp_details.serial_no,
            exp_details.exp_id,
            exp_details.date,
            exp_details.location,
            exp_details.expense_type,
            exp_details.mileage,
            exp_details.amount,
            exp_details.charge_to_fourjs,
            business_details.serial_no,
            business_details.exp_id,
            business_details.date,
            business_details.amount,
            business_details.company_name,
            business_details.business_trans,
            exp_details.mileage * C_TAX miles_total,
            exp_details.amount + exp_details.charge_to_fourjs
                total_charged_to_fourjs,
            exp_details.amount total_paid_by_employee
            FROM business_details, emp_mast, exp_details, exp_header
            WHERE exp_details.exp_id = emp_rec.exp_id
                AND exp_header.exp_id = emp_rec.exp_id
                AND emp_mast.emp_no1 = emp_rec.emp_no
            GROUP BY exp_details.serial_no

            LET total_cal =0
            LET total_cal1 = 0

    FOREACH custpay_item_tranCurr1 INTO cust_item_tranRec1.*

        IF cust_item_tranRec1.expense_type = "101" THEN
            LET cust_item_tranRec1.Entertainment = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            
            --ELSE
            --    LET cust_item_tranRec1.Entertainment = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "102" THEN
            LET cust_item_tranRec1.Meals = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --   LET cust_item_tranRec1.Meals = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "103" THEN
            LET cust_item_tranRec1.Lodging = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Lodging = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "104" THEN
            LET cust_item_tranRec1.Auto_Rent = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Auto_Rent = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "105" THEN
            LET cust_item_tranRec1.Miles = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs = 0
            --ELSE
            --    LET cust_item_tranRec1.Miles = cust_item_tranRec1.mileage
        END IF
        IF cust_item_tranRec1.expense_type = "106" THEN
            LET cust_item_tranRec1.Fuel = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Fuel = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "107" THEN
            LET cust_item_tranRec1.Transport = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Transport = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "108" THEN
            LET cust_item_tranRec1.Tolls = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Tolls = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "109" THEN
            LET cust_item_tranRec1.Phone = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Phone = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "110" THEN
            LET cust_item_tranRec1.Others = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_charged_to_fourjs=0
            --ELSE
            --    LET cust_item_tranRec1.Others = cust_item_tranRec1.amount
        END IF
        IF cust_item_tranRec1.expense_type = "111" THEN
            LET cust_item_tranRec1.Airfare = cust_item_tranRec1.amount
            LET cust_item_tranRec1.total_paid_by_employee=0
            
        END IF
        
        LET total_cal = total_cal +cust_item_tranRec1 .total_paid_by_employee
       LET total_cal1 = total_cal1 + cust_item_tranRec1.total_charged_to_fourjs

        DISPLAY "cust_item_tranRec1", cust_item_tranRec1.department
        OUTPUT TO REPORT custpay_item_report(cust_item_tranRec1.*)
    END FOREACH
    FINISH REPORT custpay_item_report

END FUNCTION

REPORT custpay_item_report(printItemsRec)

    DEFINE printItemsRec RECORD

        exp_id LIKE exp_header.exp_id,
        emp_no LIKE exp_header.emp_no,
        notes LIKE exp_header.notes,
        charge_type LIKE exp_header.charge_type,

        emp_no1 LIKE emp_mast.emp_no1,
        emp_name LIKE emp_mast.emp_name,
        position LIKE emp_mast.position,
        manager LIKE emp_mast.manager,
        department LIKE emp_mast.department,

        serial_no LIKE exp_details.serial_no,
        exp_id1 LIKE exp_details.exp_id,
        DATE LIKE exp_details.date,
        location LIKE exp_details.location,
        expense_type LIKE exp_details.expense_type,
        mileage LIKE exp_details.mileage,
        amount LIKE exp_details.amount,
        charge_to_fourjs LIKE exp_details.charge_to_fourjs,

        serial_no1 LIKE business_details.serial_no,
        exp_id2 LIKE business_details.exp_id,
        date1 LIKE business_details.date,
        amount1 LIKE business_details.amount,
        company_name LIKE business_details.company_name,
        business_trans LIKE business_details.business_trans,
        miles_total DECIMAL(9, 2),
        total_cha_to_fourjs DECIMAL(9, 2),
        total_paid_by_employee DECIMAL(9, 2),

        Entertainment VARCHAR(30),
        Meals VARCHAR(30),
        Lodging VARCHAR(30),
        Auto_Rent VARCHAR(30),
        Miles VARCHAR(30),
        Fuel VARCHAR(30),
        Transport VARCHAR(30),
        Tolls VARCHAR(30),
        Phone VARCHAR(30),
        Others VARCHAR(30),
        Airfare VARCHAR(30)

    END RECORD

    ORDER EXTERNAL BY printItemsRec.emp_name
    FORMAT
        ON EVERY ROW

            PRINTX printItemsRec.exp_id,
                printItemsRec.emp_no,
                printItemsRec.notes,
                printItemsRec.charge_type,
                printItemsRec.emp_no1,
                --printItemsRec.emp_name,
                printItemsRec.position,
                printItemsRec.manager,
                printItemsRec.department,
                printItemsRec.serial_no,
                printItemsRec.exp_id1,
                printItemsRec.DATE,
                printItemsRec.location,
                printItemsRec.expense_type,
                printItemsRec.mileage,
                printItemsRec.amount,
                printItemsRec.charge_to_fourjs,
                printItemsRec.serial_no1,
                printItemsRec.exp_id2,
                printItemsRec.date1,
                printItemsRec.amount1,
                printItemsRec.company_name,
                printItemsRec.business_trans,
                printItemsRec.miles_total,
                printItemsRec.total_cha_to_fourjs,
                printItemsRec.total_paid_by_employee,
                printItemsRec.Entertainment,
                printItemsRec.Meals,
                printItemsRec.Lodging,
                printItemsRec.Auto_Rent,
                printItemsRec.Miles,
                printItemsRec.Fuel,
                printItemsRec.Transport,
                printItemsRec.Tolls,
                printItemsRec.Phone,
                printItemsRec.Others,
                printItemsRec.Airfare

            DISPLAY "printItemsRec.emp_name ", printItemsRec.DATE
            LET createdate = DATE
            PRINTX createdate

        AFTER GROUP OF printItemsRec.emp_name
            PRINTX printItemsRec.emp_name, "Form record";

            PRINTX total_cal
           PRINTX total_cal1
            SKIP 2 LINES

END REPORT

FUNCTION expense_inpupd()
    DEFINE
        opflag CHAR(1),
        expense_cnt, curr_pa SMALLINT

    LET opflag = "U"

    LET expense_cnt = arr_expense.getLength()

    INPUT ARRAY arr_expense
        WITHOUT DEFAULTS
        FROM sa_exp.*
        ATTRIBUTE(UNBUFFERED, INSERT ROW = TRUE, AUTO APPEND = TRUE)
        BEFORE INPUT

        BEFORE ROW
            LET curr_pa = ARR_CURR()
            LET arr_expense[curr_pa].mileage = 0.575
            LET opflag = "U"

        BEFORE INSERT
            LET opflag = "I"
            SELECT MAX(serial_no) + 1
                INTO arr_expense[curr_pa].serial_no
                FROM exp_details
            IF arr_expense[curr_pa].serial_no IS NULL
                OR arr_expense[curr_pa].serial_no == 0 THEN
                LET arr_expense[curr_pa].serial_no = 1
            END IF
            LET arr_expense[curr_pa].Exp_ID = emp_rec.exp_id
            LET arr_expense[curr_pa].amount = 0
            LET arr_expense[curr_pa].date = TODAY
            -- LET arr_expense[curr_pa].total_by_fourjs = 0
            -- LET c_tax = 0.575

        AFTER INSERT
            CALL expense_insert(curr_pa)

        BEFORE DELETE
            CALL expense_delete(curr_pa)

        ON ROW CHANGE
            CALL expense_update(curr_pa)

        BEFORE FIELD Exp_ID
            IF opflag = "U" THEN
                NEXT FIELD DATE
            END IF
        AFTER FIELD expense_type
            IF (arr_expense[arr_curr()].expense_type IS NULL) THEN
                ERROR "this feild cant be null"
                NEXT FIELD expense_type
            END IF
            IF (arr_expense[arr_curr()].expense_type = 105) THEN
                NEXT FIELD mileage
            END IF
            NEXT FIELD amount

        AFTER FIELD amount
            IF arr_expense[arr_curr()].expense_type = 105 THEN
                LET arr_expense[arr_curr()].amount =
                    0.575 * arr_expense[arr_curr()].mileage
            END IF

            -- ON CHANGE mileage
            -- WHENEVER ERROR CONTINUE
            -- -- LET arr_expense[arr_curr()].miles_total =
            -- -- C_TAX * arr_expense[arr_curr()].mileage
            --
            -- WHENEVER ERROR STOP
            -- IF (SQLCA.SQLCODE = 0) THEN
            -- DISPLAY BY NAME arr_expense[arr_curr()].*
            -- --CALL DIALOG.setFieldActive("amount",FALSE)
            -- END IF

        AFTER FIELD charge_to_fourjs
            IF arr_expense[arr_curr()].charge_to_fourjs IS NULL THEN
                MESSAGE "You must enter a value"
                NEXT FIELD charge_to_fourjs
            END IF

            IF arr_expense[arr_curr()].expense_type = 111 THEN
                LET arr_expense[arr_curr()].total_charged_to_fourjs =
                    arr_expense[arr_curr()].amount
                        + arr_expense[arr_curr()].charge_to_fourjs
            ELSE
                LET arr_expense[arr_curr()].total_charged_to_fourjs =
                    arr_expense[arr_curr()].charge_to_fourjs
                DISPLAY "Total Charged ",
                    arr_expense[arr_curr()].total_charged_to_fourjs
            END IF
            -- AFTER ROW
            IF arr_expense[arr_curr()].expense_type = 111 THEN
                LET arr_expense[arr_curr()].total_paid_by_employee = 0
            ELSE
                LET arr_expense[arr_curr()].total_paid_by_employee =
                    arr_expense[arr_curr()].amount

                -- AFTER INPUT -- DISPLAY "Total Charged ",recod[arr_curr()].total_charged_to_fourjs
                -- IF NOT arr_expense[arr_curr()].expense_type = 105 THEN
                -- LET arr_expense[arr_curr()].total_paid_by_employee =
                -- arr_expense[arr_curr()].amount
                DISPLAY "Total Paid ",
                    arr_expense[arr_curr()].total_paid_by_employee
                -- WHENEVER ERROR STOP
            END IF

    END INPUT

    LET expense_cnt = arr_expense.getLength()

    IF (int_flag) THEN
        LET int_flag = FALSE
    END IF

END FUNCTION
